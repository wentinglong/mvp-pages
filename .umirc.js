const Routes = require("./src/utils/router"),
  path = require("path");

export default {
  treeShaking: true,
  routes: Routes,
  chainWebpack(config, { webpack }) {
    // 设置 alias
    config.resolve.alias.set("@", path.resolve(__dirname, "./src"));
    config.resolve.alias.set("utils", path.resolve(__dirname, "./src/utils"));
    config.resolve.alias.set("assets", path.resolve(__dirname, "./src/assets"));
    config.resolve.alias.set("components", path.resolve(__dirname, "./src/components"));
    config.resolve.alias.set("services", path.resolve(__dirname, "./src/services"));
  },
  targets: {
    ie: 11,
  },
  plugins: [
    [
      "umi-plugin-react",
      {
        antd: true,
        dva: true,
        locale: {
          enable: true,
          default: "zh-CN",
          baseNavigator: true,
        },
        dynamicImport: { webpackChunkName: true },
        title: "VE-模板编辑",
        dll: true,
        links: [
          { rel: "shortcut icon", href: "http://static.atvideo.cc/2020/05/13/11/35/favicon.ico" },
        ],
        routes: {
          exclude: [
            /models\//,
            /services\//,
            /model\.(t|j)sx?$/,
            /service\.(t|j)sx?$/,
            /components\//,
          ],
        },
      },
    ],
  ],
};
