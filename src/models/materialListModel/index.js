import commonReducers from "utils/commonReducers";
import { getData } from "services/materialList";
import Storage from "utils/Storage";

export default {
  namespace: "materialListModel",
  state: {
    page: 1,
    materialList: [],
    userMaterialList: [],
    prePage: 1000,
    currentPage: 1,
    materialLibrarySelectType: 2,
  },
  subscriptions: {
    setup({ dispatch, history }) {
      return history.listen(({ pathname }) => {
        if (pathname === "/editor") {
          dispatch({
            type: "fetch",
            payload: {
              asset_type: 2,
              page_size: 1000,
            },
          });

          // let storage = new Storage();
          //
          // dispatch({
          //   type: "setUserLocal",
          //   payload: storage.getItem("userMaterialList") || [],
          // });
        }
      });
    },
  },
  effects: {
    * fetch({ payload }, { call, put }) {
      payload.page_size = 1000;
      const { success, data } = yield  call(getData, payload);
      if (success) {
        const { asset_type } = payload;
        let storage = new Storage();
        yield put({
          type: "setUserLocal",
          payload: storage.getItem("userMaterialList") || [],
        });

        yield put({
          type: "save",
          payload: {
            materialList: data.list || [],
            materialLibrarySelectType: asset_type,
          },
        });
      }
    },
    * setUserLocal({ payload }, { put }) {
      payload = payload || [];
      yield put({
        type: "save",
        payload: {
          userMaterialList: payload,
        },
      });
    },

  },
  reducers: {
    ...commonReducers,
  },
};

