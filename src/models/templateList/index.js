import commonReducers from "utils/commonReducers";
import { getData, getTagData } from "services/templateList";


export default {
  namespace: "templateList",
  state: {
    tagList: [],
    templateList: [],
    currentPage: 1,
    category_id: "",
    total: 1,
  },
  subscriptions: {
    setup({ dispatch, history }) {
      return history.listen(({ pathname }) => {
        if (pathname === "/") {
          dispatch({
            type: "fetch",
            payload: {
              page_size: 18,
              page: 1,
              category_id: "",
            },
          });
          dispatch({
            type: "fetchTag",
          });
        }
      });
    },
  },
  effects: {
    * fetch({ payload }, { call, put }) {
      const { success, data } = yield  call(getData, payload);
      if (success) {
        yield put({
          type: "save",
          payload: {
            templateList: data.list || [],
            total: data.total,
            currentPage: payload.page,
            category_id: payload.category_id,
          },
        });
      }

    },
    * fetchTag({ payload }, { call, put }) {
      const { success, data } = yield  call(getTagData, payload);
      if (success) {
        data.category.unshift({
          category_id: "",
          category_name: "全部",
        });

        yield put({
          type: "save",
          payload: {
            tagList: data.category || [],
          },
        });
      }

    },
  },
  reducers: {
    ...commonReducers,
  },
};
