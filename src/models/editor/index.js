import commonReducers from "utils/commonReducers";
import { sourceRequest } from "utils/request";
import ParseConfig from "utils/lib/ParseConfig";
import { getImageInfo } from "utils";
import { getTemplateInfo, submitRender, getVeLeapTemplateInfo, saveVeLeapEdit } from "services/root";
import env from "@/env";
import router from "umi/router";
import { PAGE_LOADING_TYPE } from "@/types";
import { openNotification } from "utils/userPrompt";


export default {
  namespace: "editor",
  state: {
    isVeLeapTest: false,
    isShowMaterialListModel: false,
    isShowVideoClip: false,
    isFocusText: false,
    pageLoading: false,
    pageLoadingText: "正在上传文件",
    windowHeight: 0,
    windowWidth: 0,
    editWindowCurrentIndex: 1,
    isOpenMaterialList: false,
    editWindowWithPosition: true,
    horizontalVersion: false,
    parseManage: null,
    currentTemplateInfo: null,
    allMaterialByGroup: {},
    allOriginMaterialByGroup: {},
    isShowOriginMaterialContainer: false,
    backgroundImageList: {},
    prospectImageList: {},
    assetList: {},
    originAssetList: {},
    editorContainerHeight: 0,
    currentGroup: {},
    currentGroupIndex: "1",
    currentMaterialIndex: -1,
    materialDomList: {},
    currentTransformDomRef: null,
    isShowCurrentTransform: false,
    totalTime: 0,
    showStyleInfo: {
      groupHeight: 0,
      containerHeight: 0,
      containerWidth: 0,
    },
    isShowCorpImage: false,
    scale: {
      container: 1,
    },
  },
  subscriptions: {
    setup({ dispatch, history }) {
      return history.listen(({ pathname, search, query }) => {

        if (pathname === "/editor" && !search) {
          router.push("/");
        }

        if (pathname !== "/editor") {
          if (localStorage.getItem(PAGE_LOADING_TYPE) === "1") {
            window.location.reload();
            localStorage.setItem(PAGE_LOADING_TYPE, "0");
          }
        }

        if (pathname === "/editor" && search) {
          const id = query.templateId,
            veLeaUserToken = query["pid"],
            isVeLeapTest = !!veLeaUserToken;

          dispatch({
            type: "fetch",
            payload: {
              id,
              isVeLeapTest,
              token: query["v"],
              access_token: query["a"],
              veLeapProcessId: query["pid"],
            },
          });
        }

      });
    },
  },
  effects: {
    * fetch({ payload }, { call, put }) {
      const isVeLeapTest = payload.isVeLeapTest;
      let data = null,
        success = true;

      // veleap 社区的业务处理 (可以去除)
      if (payload.isVeLeapTest) {
        delete payload.isVeLeapTest;
        delete payload.id;
        payload.id = payload.veLeapProcessId;
        delete payload.veLeapProcessId;

        const templateData = yield call(getVeLeapTemplateInfo, payload);
        data = templateData.data;
        success = templateData.success;
      } else {

        payload = {
          act: "detail",
          id: payload.id,
        };
        const templateData = yield call(getTemplateInfo, payload);
        data = templateData.data;
        success = templateData.success;
      }


      if (success) {
        const { detail } = data,
          configUrl = detail["config_url"];

        const { success: getConfigSuccess, data: configData } = yield call(sourceRequest, configUrl);
        if (getConfigSuccess) {
          const parseManage = new ParseConfig(configData),
            // horizontalVersion = parseManage.horizontalVersion,
            groups = parseManage["groups"],
            backgroundImageList = JSON.parse(detail["bg_list"]),
            prospectImageList = JSON.parse(detail["fg_list"]),
            editAfterData = detail["record_json"] ? JSON.parse(detail["record_json"]) : null,
            sourceHost = env.sourceHost,
            allMaterialByGroup = {};

          let container = {},
            currentGroup = null,
            currentGroupIndex,
            groupHeight = 150,
            key,
            assetList = JSON.parse(detail["asset_list"]),
            originAssetList = null, // JSON.parse(tpl["asset_list"])
            allOriginMaterialByGroup = {},
            windowHeight = window.innerHeight - 300,
            windowWidth = window.innerWidth - 720;

          // 背景
          // for (key in backgroundImageList) {
          //   if (backgroundImageList.hasOwnProperty(key)) {
          //     backgroundImageList[key] = sourceHost + backgroundImageList[key];
          //   }
          // }
          // // 前景
          // for (key in prospectImageList) {
          //   if (prospectImageList.hasOwnProperty(key)) {
          //     prospectImageList[key] = sourceHost + prospectImageList[key];
          //   }
          // }
          // 素材链接
          for (key in assetList) {
            if (assetList.hasOwnProperty(key)) {
              if (assetList[key].indexOf("//") === -1) {
                assetList[key] = sourceHost + assetList[key];
              } else {
                assetList[key] += "?cache=1024";
              }

              let fileFormat = key.substring(key.lastIndexOf(".") + 1).toUpperCase();
              if (fileFormat === "MP4" || fileFormat === "MOV") {
                if (assetList[key].indexOf("static.atvideo.cc") !== -1) {
                  assetList[key] += "?vframe/png/offset/0";
                } else {
                  assetList[key] += "?x-oss-process=video/snapshot,t_0";
                }
              }

            }
          }
          originAssetList = JSON.parse(JSON.stringify(assetList));
          if (editAfterData && editAfterData["assetList"]) {
            assetList = editAfterData["assetList"];
          }

          // 分组
          for (key in groups) {
            if (groups.hasOwnProperty(key)) {
              const group = groups[key];
              if (editAfterData) {
                allMaterialByGroup[key] = editAfterData["allMaterialByGroup"][key];
              } else {
                allMaterialByGroup[key] = JSON.parse(JSON.stringify(group["allMaterial"]));
              }
              allOriginMaterialByGroup[key] = JSON.parse(JSON.stringify(group["allMaterial"]));
              if (!currentGroup) {
                currentGroupIndex = key;
                currentGroup = {
                  [key]: allMaterialByGroup[key],
                };
              }
            }
          }

          container = parseManage.getWidthScaleByGroup(windowHeight, currentGroupIndex);

          if (windowWidth <= container.width) {
            container = parseManage.getHeightScaleByGroup(windowWidth, currentGroupIndex);
            windowHeight = container.height;
            container = parseManage.getWidthScaleByGroup(container.height, currentGroupIndex);
          }

          yield put({
            type: "save",
            payload: {
              parseManage: parseManage,
              currentTemplateInfo: detail,
              horizontalVersion: parseManage.horizontalVersion,
              editorContainerHeight: groupHeight + 30,
              windowHeight: windowHeight,
              windowWidth: windowWidth,
              totalTime: parseManage.getTotalTime(),
              backgroundImageList: backgroundImageList,
              prospectImageList: prospectImageList,
              assetList: assetList,
              originAssetList: originAssetList,
              currentGroup: currentGroup,
              currentGroupIndex: currentGroupIndex,
              allMaterialByGroup: allMaterialByGroup,
              allOriginMaterialByGroup: allOriginMaterialByGroup,
              currentMaterialIndex: -1,
              editWindowCurrentIndex: 1,
              isOpenMaterialList: false,
              editWindowWithPosition: true,
              isChangeFontSize: false,
              pageLoading: false,
              isVeLeapTest: isVeLeapTest,
              showStyleInfo: {
                groupHeight: groupHeight,
                containerHeight: windowHeight,
                containerWidth: container.width,
              },
              scale: {
                container: container.scale,
              },
            },
          });
        }
      }


    },
    * replaceImage({ payload }, { put }) {
      const { url, assetList, assetName, i, groupKey, allMaterialByGroup, isVideo } = payload,
        { width, height } = yield getImageInfo(url);
      assetList[assetName] = url;

      let newAllMaterialByGroup = {
        ...allMaterialByGroup,
      };

      if (!newAllMaterialByGroup[groupKey][i]) {
        console.error(groupKey, i, "素材不存在!");
      }

      newAllMaterialByGroup[groupKey][i].originSize = [width, height];
      newAllMaterialByGroup[groupKey][i].anchor = [width / 2, height / 2];
      newAllMaterialByGroup[groupKey][i].isVideo = isVideo;

      yield put({
        type: "save",
        payload: {
          allMaterialByGroup: newAllMaterialByGroup,
          assetList: assetList,
          isShowCurrentTransform: false,
        },
      });

      yield put({
        type: "save",
        payload: {
          isShowCurrentTransform: true,
        },
      });
    },
    * resetCurrentMaterialTransform({ payload }, { put }) {
      const {
          allOriginMaterialByGroup, currentMaterialIndex, allMaterialByGroup,
          currentGroupIndex,
        } = payload,
        currentGroup = allOriginMaterialByGroup[currentGroupIndex];

      let newAllMaterialByGroup = {
        ...allMaterialByGroup,
      };

      let i = 0,
        len = currentGroup.length;
      for (; i < len; i++) {
        const material = currentGroup[i];
        if (material["key"] === currentMaterialIndex) {
          newAllMaterialByGroup[currentGroupIndex][i].position = currentGroup[i].position;
          newAllMaterialByGroup[currentGroupIndex][i].scale = currentGroup[i].scale;
          newAllMaterialByGroup[currentGroupIndex][i].rotate = currentGroup[i].rotate;
        }
      }

      yield put({
        type: "save",
        payload: {
          isShowCurrentTransform: false,
          allMaterialByGroup: newAllMaterialByGroup,
          currentMaterialIndex: -1,
          editWindowCurrentIndex: 1,
        },
      });

    },
    * submitRender({ payload }, { call, put }) {
      const { json } = payload;
      json.music_url = "";
      json.template_type = 1;

      const { success, data } = yield call(submitRender, json);
      if (success) {
        console.log(data);
        router.push(`/videoPlay?orderId=${data["record_id"]}&video_url=${data["video_url"]}`);
      }
    },
    * veLeapTestSubmitRender({ payload }, { call }) {
      const { json, veLeapToken, pid, i, test_adopt, project_id, process_id } = payload;
      const { success, data } = yield call(submitRender, json);
      if (success) {
        router.push(`/veleapTestVideoPlay?orderId=${data.res.data["orderId"]}&process_id=${process_id}&v=${veLeapToken}&pid=${pid}&i=${i}&test_adopt=${test_adopt}&project_id=${project_id}`);
      }
    },
    * veLeapTestSave({ payload }, { call }) {
      const { success } = yield call(saveVeLeapEdit, payload);
      if (success) {
        openNotification("保存成功", "修改已保存至草稿箱", "success");
      }

    },
  },
  reducers: {
    ...commonReducers,
    changeGroupData(state, { payload }) {
      const { allMaterialByGroup, currentGroupIndex, currentMaterialIndex } = state;
      let currentMaterialGroup = allMaterialByGroup[currentGroupIndex],
        i = 0,
        len = currentMaterialGroup.length,
        currentMaterial;

      for (; i < len; i++) {
        currentMaterial = currentMaterialGroup[i];

        if (currentMaterialIndex === currentMaterial["key"]) {
          const { type, data } = payload;
          if (type === "position") {
            let newX = currentMaterial.position[0] + data.offsetX,
              newY = currentMaterial.position[1] + data.offsetY;
            currentMaterial.position = [newX, newY];

            if (!currentMaterial["distPosition"]) {
              currentMaterial["distPosition"] = [0, 0];
            }
            currentMaterial["distPosition"] = [
              currentMaterial["distPosition"][0] + data.offsetX,
              currentMaterial["distPosition"][1] + data.offsetY,
            ];
          }

          if (type === "scale") {
            let newScaleX = currentMaterial.scale[0] * data["offsetScaleX"],
              newScaleY = currentMaterial.scale[1] * data["offsetScaleY"];
            currentMaterial.scale = [newScaleX, newScaleY];

            if (!currentMaterial["distScale"]) {
              currentMaterial["distScale"] = [1, 1];
            }
            currentMaterial["distScale"] = [
              currentMaterial["distScale"][0] * data["offsetScaleX"],
              currentMaterial["distScale"][1] * data["offsetScaleY"],
            ];

          }

          if (type === "rotate") {
            currentMaterial.rotate = currentMaterial.rotate + data.rotate;
            if (!currentMaterial["distRotate"]) {
              currentMaterial["distRotate"] = 0;
            }
            currentMaterial["distRotate"] += data.rotate;
          }

          if (type === "reset") {
            currentMaterial["distRotate"] = 0;
            currentMaterial["distScale"] = [1, 1];
            currentMaterial["distPosition"] = [0, 0];
          }

          if (type === "changeText") {
            currentMaterial["fontSetting"]["defaultText"] = data.text;
          }

          if (type === "changeVolume") {
            currentMaterial["volume"] = data.volume;
          }

          if (type === "changeFontSize") {
            currentMaterial["fontSetting"]["fontSize"] = data.size;
          }

          if (type === "changeTextColor") {
            currentMaterial["fontSetting"]["textColor"] = data.color;
          }

          if (type === "changeStrokeColor") {
            currentMaterial["fontSetting"]["strokeColor"] = data.color;
          }

          if (type === "changeStrokeWidth") {
            currentMaterial["fontSetting"]["strokeWidth"] = data.width;
          }

          if (type === "videoClip") {
            currentMaterial["videoClip"] = data;
          }

        }
      }

      return {
        ...state,
        allMaterialByGroup: allMaterialByGroup,
      };
    },
  },
};
