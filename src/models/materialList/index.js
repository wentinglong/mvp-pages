import commonReducers from "utils/commonReducers";
import { getData } from "services/materialList";

export default {
  namespace: "materialList",
  state: {
    page: 1,
    materialList: [],
    prePage: 1000,
    currentPage: 1,
    materialLibrarySelectType: 1,
  },
  subscriptions: {
    setup({ dispatch, history }) {
      return history.listen(({ pathname }) => {
        if (pathname === "/editor") {
          dispatch({
            type: "fetch",
            payload: {
              asset_type: 1,
              page_size: 1000,
            },
          });
        }
      });
    },
  },
  effects: {
    * fetch({ payload }, { call, put }) {
      payload.page_size = 1000;
      const { success, data } = yield  call(getData, payload);
      if (success) {
        const { asset_type } = payload;
        yield put({
          type: "save",
          payload: {
            materialList: data.list || [],
            materialLibrarySelectType: asset_type,
          },
        });
      }

    },
  },
  reducers: {
    ...commonReducers,
  },
};
