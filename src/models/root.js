import commonReducers from "utils/commonReducers";
import Cookies from "js-cookie";
import { getToken } from "services/root";
import { TOKEN_NAME, SOURCE_URL } from "@/types";
import { getData } from "services/templateList";

export default {
  namespace: "root",
  state: {},
  subscriptions: {
    // setup({ dispatch, history }) {
    //   return history.listen(({ pathname }) => {
    //     if (!Cookies.get(TOKEN_NAME)) {
    //       dispatch({
    //         type: "fetch",
    //         payload: {
    //           pathname: pathname,
    //         },
    //       });
    //     } else {
    //       dispatch({
    //         type: "checkToken",
    //         payload: {
    //           asset_type: 1,
    //         },
    //       });
    //     }
    //   });
    // },

  },
  effects: {
    * fetch({ payload }, { call }) {
      const { success, data } = yield call(getToken);
      if (success) {
        Cookies.set(TOKEN_NAME, data["token"]);
        Cookies.set(SOURCE_URL, data["source_url"]);
        window.location.reload();
      }
    },
    * checkToken({ payload }, { call }) {
      const { success, errno } = yield  call(getData, payload);
      if (!success) {
        if (errno === 40015) {
          Cookies.remove(TOKEN_NAME);
          window.location.reload();
        }
      }
    },

  },
  reducers: {
    ...commonReducers,
  },

};
