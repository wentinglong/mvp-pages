import requestMiddleLayer from "./requestMiddleLayer";
import { sourceRequest as jsonRequest } from "./request";

export const getImageInfo = (url) => {
  const img = document.createElement("img");
  img.crossOrigin = "anonymous";
  img.src = url;
  return new Promise(resolve => {
    img.addEventListener("load", function() {
      resolve({
        image: this,
        width: this.width,
        height: this.height,
      });
    });

    setTimeout(() => {
      resolve("获取信息失败!");
    }, 5000);

  });
};

export const getVideoInfo = (url) => {
  const video = document.createElement("video");
  video.src = url;
  return new Promise(resolve => {
    video.addEventListener("canplay", function() {
      resolve({
        width: this.videoWidth,
        height: this.videoHeight,
        duration: this.duration,
      });
    });

    setTimeout(() => {
      resolve("获取信息失败!");
    }, 5000);

  });
};

export const getImage = (url) => {
  const img = document.createElement("img");
  img.crossOrigin = "anonymous";
  img.src = url;

  return new Promise(resolve => {
    img.addEventListener("load", function() {
      resolve(img);
    });
  });
};


export const _ = requestMiddleLayer;
export const sourceRequest = jsonRequest;
