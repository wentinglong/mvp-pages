import request from "./request";

const requestMiddleLayer = (url, data = {}, method = "post", isVeLeap) => request({
  url: url,
  method: method,
  data: data,
  isVeLeap: isVeLeap,
});

export default requestMiddleLayer;

