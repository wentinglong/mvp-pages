import env from "../env.js";
import axios from "axios";
import Cookie from "js-cookie";
import qs from "qs";
import { openNotification } from "./userPrompt";
import { TOKEN_NAME } from "@/types";


const request = (options) => {
  let {
      url, method = "get", data, isVeLeap = false,
    } = options,
    jwt = Cookie.get(TOKEN_NAME) || "",
    fullUrl = "",
    isFullUrl = false;

  method = method.toUpperCase();
  if (
    url.indexOf("http") !== -1 ||
    url.indexOf("https") !== -1 ||
    url.indexOf("//") !== -1
  ) {
    isFullUrl = true;
  }


  if (method === "GET" && Object.keys(data).length > 0) url += "?" + qs.stringify(data);
  if (!isFullUrl) {
    fullUrl = `${env.host}${url}`;
    if (isVeLeap) {
      fullUrl = `${env.veLeapHost}${url}`;
    }
  } else {
    fullUrl = url;
  }


  return axios({
    method: method,
    url: fullUrl,
    data: data,
    headers: {
      "Content-Type": "application/json",
      "login-token": jwt,
    },
  }).then(response => {
    let { status, statusText, data } = response,
      result = {
        success: true,
        message: statusText,
        statusCode: status,
        ...data,
      };

    if (status >= 200 && status <= 304 && data.errno <= 0) {
      return Promise.resolve(result);
    } else {
      result.success = false;
      result.message = data["errormsg"];
      return Promise.reject(result);
    }

  }).catch((error) => {
    //错误处理
    console.dir(error);
    openNotification("网络错误", error.message, "error");
    if (error.errno) {
      return Promise.resolve(error);
    }
  });

};

export const sourceRequest = (url, type) => {
  let fullUrl = "";
  if (
    url.indexOf("http") !== -1 ||
    url.indexOf("https") !== -1 ||
    url.indexOf("//") !== -1
  ) {
    fullUrl = url;
  } else {
    fullUrl = `${env.sourceHost}${url}`;
  }


  type = type || "json";
  if (type === 1) type = "arraybuffer";

  return axios({
    url: fullUrl,
    responseType: type,
  }).then(response => {
    let { status, data } = response;
    if (status >= 200 && status <= 304) {
      return Promise.resolve({
        success: true,
        status,
        data,
      });
    }

    return Promise.reject({
      success: false,
      status,
      data,
    });

  }).catch((error) => {
    //错误处理
    console.dir(error);
    openNotification("请求资源错误!", error.message, "error");
  });

};

export default request;
