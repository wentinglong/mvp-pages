class Storage {

  constructor(name) {
    this.name = "storage";
  }

  setItem(params) {
    let obj = {
      name: "",
      value: "",
      expires: "",
      startTime: new Date().getTime(),
    };
    let options = {};

    Object.assign(options, obj, params);
    if (options.expires) {

      localStorage.setItem(options.name, JSON.stringify(options));
    } else {

      let type = Object.prototype.toString.call(options.value);

      if (type === "[object Object]") {
        options.value = JSON.stringify(options.value);
      }
      if (type === "[object Array]") {
        options.value = JSON.stringify(options.value);
      }
      localStorage.setItem(options.name, options.value);
    }
  }

  getItem(name) {
    let item = localStorage.getItem(name);
    if (!item) return null;
    try {
      item = JSON.parse(item);
    } catch (error) {
      console.log(error);
      console.log(item);
    }
    if (item.startTime) {
      let date = new Date().getTime();

      if (date - item.startTime > item.expires) {

        localStorage.removeItem(name);
        return false;
      } else {

        return item.value;
      }
    } else {

      return item;
    }
  }


  removeItem(name) {
    localStorage.removeItem(name);
  }

  clear() {
    localStorage.clear();
  }

}

export default Storage;
