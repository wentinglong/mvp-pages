const reducersObject = {
  save(state, { payload }) {
    return { ...state, ...payload };
  },
};

export default reducersObject;
