let routes = [
  {
    path: "/",
    component: "../layouts",
    routes: [
      {
        path: "/",
        component: "templateList",
      },
      {
        path: "/editor",
        component: "editor",
      },
      {
        path: "/videoPlay",
        component: "videoPlay",
      },
      {
        path: "/veLeapTestVideoPlay",
        component: "veLeapTestVideoPlay",
      },
    ],
  },
];

module.exports = routes;
