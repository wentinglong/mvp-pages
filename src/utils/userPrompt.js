import { notification } from "antd";

export function openNotification(title, des, type, duration = 4) {
  notification[type]({
    message: title,
    description: des,
    duration: duration,
  });
}
