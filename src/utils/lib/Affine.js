/**
 * 仿射变换矩阵支持
 *
 * Created by slayer on 18-12-26.
 */

class Affine {

  constructor() {
    this.mA = 1;
    this.mB = 0;
    this.mC = 0;
    this.mD = 1;
    this.mE = 0;
    this.mF = 0;
  }

  setElement(_mA, _mB, _mC, _mD, _mE, _mF) {
    this.mA = _mA;
    this.mB = _mB;
    this.mC = _mC;
    this.mD = _mD;
    this.mE = _mE;
    this.mF = _mF;
  }

  cloneFrom(originAffine) {
    this.mA = originAffine.mA;
    this.mB = originAffine.mB;
    this.mC = originAffine.mC;
    this.mD = originAffine.mD;
    this.mE = originAffine.mE;
    this.mF = originAffine.mF;
  }


  set(anchor, position, scale, rotation) {
    let _rotation = rotation * 0.017453292519943;
    let cs = Math.cos(_rotation);
    let sn = Math.sin(_rotation);

    this.mA = cs * scale.x;
    this.mB = sn * scale.x;
    this.mC = -sn * scale.y;
    this.mD = cs * scale.y;

    this.mE = position.x;
    this.mF = position.y;

    this.translate(-anchor.x, -anchor.y);
  }

  translate(tx, ty) {
    let t = new Affine();
    t.setTranslate(tx, ty);
    this.premultiply(t);
  }


  setTranslate(tx, ty) {
    this.mA = this.mD = 1;
    this.mB = this.mC = 0;
    this.mE = tx;
    this.mF = ty;
  }

  premultiply(t) {
    t.multiply(this);
    this.mA = t.mA;
    this.mB = t.mB;
    this.mC = t.mC;
    this.mD = t.mD;
    this.mE = t.mE;
    this.mF = t.mF;
  }

  multiply(other) {
    let t0 = this.mA * other.mA + this.mB * other.mC;
    let t2 = this.mC * other.mA + this.mD * other.mC;
    let t4 = this.mE * other.mA + this.mF * other.mC + other.mE;

    this.mB = this.mA * other.mB + this.mB * other.mD;
    this.mD = this.mC * other.mB + this.mD * other.mD;
    this.mF = this.mE * other.mB + this.mF * other.mD + other.mF;

    this.mA = t0;
    this.mC = t2;
    this.mE = t4;
  }


  getInverse() {
    let inv_det;
    let det = this.mA * this.mD - this.mC * this.mB;
    if (det > -0.000001 && det < 0.000001) {
      return false;
    }
    inv_det = 1.0 / det;

    let out = new Affine();
    out.mA = this.mD * inv_det;
    out.mC = -this.mC * inv_det;
    out.mE = (this.mC * this.mF - this.mD * this.mE) * inv_det;
    out.mB = -this.mB * inv_det;
    out.mD = this.mA * inv_det;
    out.mF = (this.mB * this.mE - this.mA * this.mF) * inv_det;

    return out;
  }


  transform(position) {
    let pos = {};
    pos.x = this.mA * position.x + this.mC * position.y + this.mE;
    pos.y = this.mB * position.x + this.mD * position.y + this.mF;

    return pos;
  }
}

export default Affine;
