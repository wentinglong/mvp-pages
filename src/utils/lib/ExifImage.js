class ExifImage {

  /**
   * 构造函数
   * @param imageElement {HTMLImageElement}
   * @param type {Number}
   * */
  constructor(imageElement, type) {
    this.imageElement = imageElement;
    this.type = type;
    //存放宽高
    this.rectList = [];
    this.canvasElement = this.createCanvasElement();
    this.context = this.getCanvasContext();
  }

  /**
   * 获取修复后照片
   * */
  getAfterRepairImage() {
    const { imageElement } = this;
    if (!imageElement.complete) {
      throw new Error("image the load is not complete!");
    }
    return this.getImage();
  }

  /**
   * 获取修复后照片
   * */
  getImage() {
    const { imageElement, type, rectList, canvasElement, context } = this;
    let rotate = 0,
      x = 0,
      y = 0,
      width = 0,
      height = 0;

    rectList[0] = imageElement.height;
    rectList[1] = imageElement.width;
    width = rectList[1];
    height = rectList[0];


    switch (type) {
      case 3:
        rotate = -180 * Math.PI / 180;
        rectList[0] = imageElement.width;
        rectList[1] = imageElement.height;
        x = rectList[0];
        y = rectList[1];
        width = rectList[0];
        height = rectList[1];
        break;

      case 6:
        rotate = -90 * Math.PI / 180;
        x = 0;
        y = rectList[1];
        break;

      case 8:
        rotate = 90 * Math.PI / 180;
        x = rectList[0];
        y = 0;
        break;

      default:
        console.error(`invalid type ${type}!`);
        break;
    }


    canvasElement.width = rectList[0];
    canvasElement.height = rectList[1];
    context.translate(x, y);
    context.rotate(rotate);
    context.drawImage(imageElement, 0, 0, width, height);
    return canvasElement;
  }

  /**
   * 创建 canvas 标签
   * */
  createCanvasElement() {
    const canvas = document.createElement("canvas");
    canvas.width = 0;
    canvas.height = 0;
    return canvas;
  }

  /**
   * 获取 context
   * */
  getCanvasContext() {
    return this.canvasElement.getContext("2d");
  }

}

export default ExifImage;
