import axios from "axios";

class Upload {

  constructor(options) {
    this.init(options);
  }

  init(options) {
    this.initialization();
    const { getTokenUrl, userDir, jwt, sourceHost } = options;
    this.getTokenUrl = getTokenUrl;
    this.userDir = userDir || "";
    this.jwt = jwt;
    this.sourceHost = sourceHost;
  }

  initialization() {
    this.getTokenUrl = "";
    this.userDir = "";
    this.jwt = "";
    this.sourceHost = "";
  }

  getFileType(fileName) {
    let point = fileName.lastIndexOf(".");
    return fileName.substr(point);
  }

  hexRandomStr() {
    const s = [],
      hexDigits = "0123456789abcdef";
    for (let i = 0; i < 36; i++) {
      s[i] = hexDigits.substr(Math.floor(Math.random() * 0x10), 1);
    }
    s[14] = "4";
    s[19] = hexDigits.substr((s[19] & 0x3) | 0x8, 1);
    return s.join("");
  }

  getUploadPath(fileName, dir) {
    const userDir = this.userDir || dir;

    return userDir + (new Date()).getTime() + this.hexRandomStr() + this.hexRandomStr() + this.getFileType(fileName);
  }

  getToken() {
    const { getTokenUrl, jwt } = this;
    return axios({
      url: getTokenUrl,
      headers: {
        "login-token": jwt,
      },
    }).then(res => {

      let { data } = res;
      return {
        data: {
          policy: data,
        },
      };
    });
  }

  getFormData(policy, path, file) {
    let formData = new FormData();
    formData.append("key", path);
    formData.append("OSSAccessKeyId", policy["accessid"]);
    formData.append("policy", policy["policy"]);
    formData.append("Signature", policy.signature);
    formData.append("callback", policy.callback);
    formData.append("success_action_status", 200); // 成功后返回的操作码
    formData.append("file", file);
    return formData;
  }

  async uploadFile(file) {
    let { data: { policy } } = await this.getToken(),
      path = this.getUploadPath(file.name, policy.dir),
      formData = this.getFormData(policy, path, file);

    await axios({
      url: `//${policy["host"]}`,
      method: "POST",
      data: formData,
    });

    return `${this.sourceHost}${path}`;
  }

}

export default Upload;
