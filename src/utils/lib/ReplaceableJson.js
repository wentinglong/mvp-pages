class ReplaceableJson {

  constructor(allMaterialByGroup, orderList, templateId) {
    this.init(allMaterialByGroup, orderList, templateId);
  }

  init(allMaterialByGroup, orderList, templateId) {
    allMaterialByGroup = allMaterialByGroup || {};
    orderList = orderList || [];
    this.initialization();
    this.allMaterialByGroup = JSON.parse(JSON.stringify(allMaterialByGroup));
    this.orderList = JSON.parse(JSON.stringify(orderList));
    this.resultJson.tpl_id = templateId;
    this.mapOrderList();
  }

  initialization() {
    this.allMaterialByGroup = null;
    this.orderList = null;
    this.templateId = -1;
    this.resultJson = {
      replaceableJson: [],
    };
  }

  getMaterialByKey(key) {
    const { allMaterialByGroup } = this;
    let groupKey;
    for (groupKey in allMaterialByGroup) {
      if (Object.hasOwnProperty.call(allMaterialByGroup, groupKey)) {
        const currentGroup = allMaterialByGroup[groupKey];
        let i = 0,
          len = currentGroup.length;
        for (; i < len; i++) {
          const material = currentGroup[i];
          if (material["key"] === key) {
            return material;
          }

        }

      }
    }

    return null;

  }

  mapOrderList() {
    const { orderList } = this;
    let i = 0,
      len = orderList.length;
    for (; i < len; i++) {
      let material = this.getMaterialByKey(orderList[i]),
        {
          type, isVideo, imgUrl, fontSetting, adapt_type, videoTransform,
          volume, videoClip,
        } = material,
        asset = {
          type: type,
        };

      videoClip = videoClip || {
        startTime: -1,
        endTime: -1,
      };

      switch (type) {
        case 1:
          if (isVideo) {
            if (imgUrl.indexOf("http:") === -1) {
              asset["main_file"] = `http:${imgUrl}`;
            } else {
              asset["main_file"] = imgUrl;
            }

            asset["attr"] = {
              "adapt_type": adapt_type,
              "transform": videoTransform,
              "volume": volume / 100,
              "clip_start": videoClip.startTime,
              "clip_end": videoClip.endTime,
            };
            if (typeof volume === "undefined") {
              delete asset["attr"]["volume"];
            }
          } else {
            asset["main_file"] = `http:${imgUrl}`;
          }
          break;

        case 2:
        case 3:
          const {
            defaultText, fontSize, textColor, strokeColor, strokeWidth,
          } = fontSetting;
          asset["attr"] = {
            text: defaultText,
            fill: textColor,
            stroke: strokeColor,
            stroke_width: strokeWidth,
            size: fontSize,
          };
          break;

        default:
          break;
      }
      asset["type"] = type;
      this.resultJsonPush(asset);
    }

  }

  resultJsonPush(item) {
    this.resultJson.replaceableJson.push(item);
  }

  getJson() {
    return this.resultJson;
  }

}

export default ReplaceableJson;
