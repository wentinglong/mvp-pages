/**
 * config 文件解析
 * */

class ParseConfig {

  /**
   * 构造函数
   * @param config {Object}
   * */
  constructor(config) {

    this.init(config);
  }

  /**
   * 初始化函数
   * @param config {Object}
   * */
  init(config) {
    this.initialization();
    this.configJson = JSON.parse(JSON.stringify(config));
    this.templateWidth = this.configJson["size"][0];
    this.templateHeight = this.configJson["size"][1];
    this.uiGroupSize = this.configJson["ui_group"];
    this.fps = this.configJson["fps"];
    this.horizontalVersion = this.templateWidth > this.templateHeight;
    this.groupingByAssetKey(this.configJson.assets);
    this.groupingByAssetUiType();
  }

  /**
   * 变量初始化
   * */
  initialization() {
    //configJson
    this.configJson = {};
    //带ui字段的 assets
    this.uiAssets = {};
    //顺序数组
    this.orderList = [];
    //编辑组 键列表
    this.groupKeys = {};
    //编辑组
    this.groups = {};
    //编辑组 size
    this.uiGroupSize = [];
    //帧率
    this.fps = 30;
    //替换点个数
    this.counter = {
      totalCount: 0,
      imageCount: 0,
      textCount: 0,
    };
    this.templateWidth = this.templateHeight = 0;
    this.horizontalVersion = false;
    this.totalTime = 0;
  }

  /**
   *  获取总时长
   * @return totalTime {Number}
   * */
  getTotalTime() {
    this.totalTime = Math.ceil(this.configJson.duration / this.configJson["fps"]);
    return this.totalTime;
  }

  /**
   * 根据宽度获取比例 和 高度
   * @param width {Number}
   * @return scaleInfo {Object}
   * */
  getHeightScale(width) {
    let scale = width / this.templateWidth,
      height = this.templateHeight * scale;

    return {
      scale: scale,
      height: height,
    };
  }

  /**
   * 根据高度获取比例 和 宽度
   * @param height {Number}
   * @return scaleInfo {Object}
   * */
  getWidthScale(height) {
    let scale = height / this.templateHeight,
      width = this.templateWidth * scale;
    return {
      scale: scale,
      width: width,
    };
  }

  /**
   * 根据高度获取编辑器和组的 比例 和 宽度
   * @param height {Number}
   * @param groupKey {Number|String}
   * @return scaleInfo {Object}
   * */
  getWidthScaleByGroup(height, groupKey) {
    let sizeArray = [this.templateWidth, this.templateHeight];
    if (this.groups[groupKey]) {
      sizeArray = this.groups[groupKey]["groupSize"];
    }

    let scale = height / sizeArray[1],
      width = sizeArray[0] * scale;
    return {
      scale: scale,
      width: width,
    };
  }

  /**
   * 根据高度获取编辑器和组的 比例 和 宽度
   * @param width {Number}
   * @param groupKey {Number|String}
   * @return scaleInfo {Object}
   * */
  getHeightScaleByGroup(width, groupKey) {
    let sizeArray = [this.templateWidth, this.templateHeight];
    if (this.groups[groupKey]) {
      sizeArray = this.groups[groupKey]["groupSize"];
    }

    let scale = width / sizeArray[0],
      height = sizeArray[1] * scale;

    return {
      scale: scale,
      height: height,
    };
  }

  /**
   * 根据带有 ui asset 的key 分组
   * @param assets {Array}
   * */
  groupingByAssetKey(assets) {
    let textCount = 0,
      imageCount = 0,
      totalCount = 0;

    this.forEach(assets, currentAssets => {
      if (currentAssets.ui) {
        const { ui: { group, type }, key } = currentAssets;
        this.uiAssets[key] = currentAssets;
        this.orderList.push(key);
        if (!this.groupKeys[group]) {
          this.groupKeys[group] = [];
        }
        this.groupKeys[group].push(key);
        if (type === 1) imageCount++;
        else textCount++;
        totalCount++;
      }
    });

    this.counter.textCount = textCount;
    this.counter.imageCount = imageCount;
    this.counter.totalCount = totalCount;
  }

  /**
   * groupingByAssetKey 分组过后,根据 ui type 细分类型
   * */
  groupingByAssetUiType() {
    this.forEach(this.groupKeys, (groupKeyList, i) => {
      const groupSize = this.uiGroupSize[i - 1] || {};
      this.groups[i] = {
        textMaterial: [],
        imageMaterial: [],
        characterAnimationMaterial: [],
        unknownMaterial: [],
        backgroundImage: "",
        prospectImage: "",
        allMaterial: [],
        groupSize: groupSize.size,
      };

      this.forEach(groupKeyList, groupKey => {
        const currentAssets = this.uiAssets[groupKey],
          assetsUi = currentAssets["ui"],
          group = {
            asset: currentAssets,
            key: currentAssets["key"],
            assetName: currentAssets["name"],
            originSize: assetsUi["editSize"],
            position: assetsUi["p"],
            scale: assetsUi["s"],
            transparence: assetsUi["t"],
            rotate: assetsUi["r"],
            zIndex: assetsUi["index"],
            clickArea: assetsUi["area"],
            type: assetsUi["type"],
            anchor: assetsUi["a"],
          };


        let isVideo = false;
        if (assetsUi.b) {
          this.groups[i]["backgroundImage"] = assetsUi.b;
        }

        if (assetsUi.f) {
          this.groups[i]["prospectImage"] = assetsUi.f;
        }

        switch (assetsUi.type) {
          case 1:
            this.groups[i]["imageMaterial"].push(group);
            let fileFormat = group.assetName.substring(group.assetName.lastIndexOf(".") + 1).toUpperCase();
            if (fileFormat === "MP4" || fileFormat === "MOV") {
              isVideo = true;
            }
            group.isVideo = isVideo;
            break;

          case 2:
          case 3:
            group.fontSetting = {
              defaultText: assetsUi["default"],
              fontFamily: assetsUi["font"],
              fontSize: assetsUi["size"],
              lineHeight: assetsUi["line_height"],
              strokeWidth: assetsUi["width"],
              strokeColor: assetsUi["stroke"],
              textAlign: assetsUi["align"],
              textDirection: assetsUi["direction"],
              textColor: assetsUi["fill"],
              maxLength: assetsUi["max"],
            };
            assetsUi.type === 2 && this.groups[i]["textMaterial"].push(group);
            assetsUi.type === 3 && this.groups[i]["characterAnimationMaterial"].push(group);
            break;

          default:
            this.groups[i]["unknownMaterial"].push(group);
            break;
        }
        // console.log(group);
        this.groups[i]["allMaterial"].push(group);

      });
    });
  }

  /**
   * 对象|数组 遍历
   * @param list {Array|Object}
   * @param callback {Function}
   * */
  forEach(list, callback) {
    const isFn = typeof callback !== "function",
      isArray = Array.isArray(list);

    if (isFn) throw new Error("callback is not a function!");
    let i = 0,
      keys = [],
      len = list.length;

    if (!isArray) {
      keys = Object.keys(list);
      len = keys.length;
    }

    for (; i < len; i++) {
      isArray ? callback(list[i], i) : callback(list[keys[i]], keys[i]);
    }
  }

}

export default ParseConfig;
