/**
 * 截图组件
 *
 * */
import ExifImage from "./ExifImage";
import Affine from "./Affine";

class Cropper {

  constructor(options) {
    options = options || { width: 720, height: 1280 };
    this.init(options);
    this.initialization();
  }

  /**
   * 初始化函数
   * */
  init(options) {
    const { width, height } = options;
    this.width = width;
    this.height = height;
    this.canvasElement = this.createCanvasElement();
    this.context = this.getContext();
  }

  /**
   * 变量初始化函数
   * */
  initialization() {

    // complete 布尔值表示图片是否加载完成
  }

  /**
   * 设置矩阵
   * */
  setAffine(matrix, moveMatrix) {
    const { anchor, position, zoom, rotation } = matrix;
    let origin = new Affine();
    origin.set(anchor, position, zoom, rotation);
    moveMatrix && origin.multiply(moveMatrix);
    this.context.setTransform(origin.mA, origin.mB, origin.mC, origin.mD, origin.mE, origin.mF);
  }

  /**
   * 绘制图片
   * @param image {HTMLImageElement}
   * @param imageWidth {Number}
   * @param imageHeight {Number}
   * @param imagePositionX {Number}
   * @param imagePositionY {Number}
   * */
  drawImage(image, imagePositionX, imagePositionY, imageWidth, imageHeight) {
    return new Promise(resolve => {
      window.EXIF.getData(image, () => {
        let orientation = window.EXIF.getTag(image, "Orientation"),
          newCanvas;
        if (orientation && orientation !== 1) {
          let exifImage = new ExifImage(image, orientation);
          newCanvas = exifImage.getAfterRepairImage();
        } else {
          newCanvas = image;
        }
        this.canvasSave();
        this.context.drawImage(newCanvas, imagePositionX, imagePositionY, imageWidth, imageHeight);
        this.canvasRestore();
        resolve();
      });
    });
  }

  /**
   * 获取 canvas blob 数据
   * @return {Promise}
   * */
  getBlobData() {
    return new Promise(resolve => {
      this.canvasElement.toBlob(function(blob) {
        resolve(blob);
      });
    });
  }

  /**
   * 创建 canvas 标签
   * */
  createCanvasElement() {
    const canvas = document.createElement("canvas");
    canvas.width = this.width;
    canvas.height = this.height;
    return canvas;
  }

  /**
   * 获取context
   * */
  getContext() {
    return this.canvasElement.getContext("2d");
  }

  /**
   * 保存 canvas 当前状态
   * */
  canvasSave() {
    this.context.save();
  }

  /**
   * 回滚到最近一次保存的状态
   * */
  canvasRestore() {
    this.context.restore();
  }

}

export default Cropper;
