# 文件说明
- Affine js 2D 矩阵运算类 文档链接: http://vesdk.com/docs/page_145.html
- Cropper canvas 截图类
- Cropper 截图的辅助类 用于修复旋转的图片
- ParseConfig 解析类 用于解析 config.json 文档链接: http://vesdk.com/docs/page_107.html
- ReplaceableJson 创建替换结构Json 用于后端渲染 文档链接: http://vesdk.com/docs/page_103.html
