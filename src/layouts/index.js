import React from "react";
import { connect } from "dva";
import styles from "./index.less";
import VE_logo from "assets/img/VE_logo.svg";

function BasicLayout(props) {

  return (
    <div className={`${styles.basicLayout}`}>
      <div className={`flex borderBox ${styles.fixedHeader}`}>
        <a href="/" rel="noreferrer" className={`flex ${styles.logoBox}`}>
          <img src={VE_logo} className={styles.logo} alt="logo"/>
          <h1 className={styles.title}>VE demo</h1>
        </a>
      </div>
      {props.children}
    </div>
  );

}

const stateToProps = state => state["root"];

export default connect(stateToProps)(BasicLayout);
