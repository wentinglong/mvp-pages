import { Collapse } from "antd";
import React, { PureComponent } from "react";
import { connect } from "dva";
import styles from "./index.less";
const { Panel } = Collapse;
class LayerList extends PureComponent {
  render() {
    const list = [
      {
        title: "111111111",
        key: "1",
        text: ["111", "222"],
      },
      {
        title: "222222222",
        key: "2",
        text: ["222", "222"],
      },
      {
        title: "3333333",
        key: "3",
        text: ["333", "222"],
      },
    ];

    const menu = list.map(item => (
      <Panel header={item.title} key={item.key}>
        {item.text.map(option => (
          <div>
            <img
              className={styles.img}
              src="//web-demo-source.atvideo.cc/web-demo-public/2f988e49bc83fe8ac671237b8b8779f1/ui/ui0.png"
            />
            <div>{option}</div>
          </div>
        ))}
      </Panel>
    ));

    return (
      <div className={styles.layerList}>
        <div className={styles.layerTitle}>
          <span>图层</span> <img src="#" />{" "}
        </div>
        <Collapse accordion>{menu}</Collapse>
      </div>
    );
  }
}

const stateToProps = state => {
  return {
    editor: state["editor"],
    loading: state["loading"],
  };
};

export default connect(stateToProps)(LayerList);
