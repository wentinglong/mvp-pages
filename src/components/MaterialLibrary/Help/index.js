import React, { useState } from "react";
import styles from "./index.less";


function Help() {

  const [helpVisible, setHelpVisible] = useState(false);

  const handleHelpIconClick = (status) => {
    setHelpVisible(status);
  };

  const renderProps = {
    shortcutKeyIsShow: helpVisible ? "visibilityShow" : "visibilityHidden",
  };

  return (
    <div className={`${styles.helpContainer}`}>
        <span
          onClick={() => handleHelpIconClick(!helpVisible)}
          className={`iconfont cursorPointer ${styles.helpIcon}`}
        >&#xe68f;
        </span>
      {/* 快捷键列表 */}
      <div className={`flex ${styles.shortcutKey} ${renderProps.shortcutKeyIsShow}`}>
        <h4 className={`flex borderBox ${styles.shortcutKeyTitle}`}>
          快捷键
          <span
            className={`iconfont cursorPointer ${styles.shortcutKeyTitleClose}`}
            onClick={() => handleHelpIconClick(!helpVisible)}
          >
              &#xe67f;
            </span>
        </h4>
        <ul className={`${styles.shortcutKeyList}`}>
          <li className={`flex borderBox ${styles.shortcutKeyOptions}`}>
            <span>上移</span>
            <span>↑</span>
          </li>
          <li className={`flex borderBox ${styles.shortcutKeyOptions}`}>
            <span>下移</span>
            <span>↓</span>
          </li>
          <li className={`flex borderBox ${styles.shortcutKeyOptions}`}>
            <span>左移</span>
            <span>←</span>
          </li>
          <li className={`flex borderBox ${styles.shortcutKeyOptions}`}>
            <span>右移</span>
            <span>→</span>
          </li>
        </ul>
      </div>
    </div>
  );

}


export default Help;
