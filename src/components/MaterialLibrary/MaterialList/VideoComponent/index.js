import React, { useEffect, useRef, useState } from "react";

function VideoComponent(props) {

  const { url, videoTimeStyle, videoBoxStyle } = props,
    videoDOM = useRef(),
    [duration, setDuration] = useState(0);

  useEffect(() => {
    const { current } = videoDOM;
    current.addEventListener("canplay", () => {
      setDuration(current["duration"]);
    });
  }, []);


  const videoPlay = () => {
    const { current } = videoDOM;
    if (!current || (current && !current.paused && current.readyState === 4)) {
      return false;
    }
    current.play();
  };

  const videoPause = () => {
    const { current } = videoDOM;
    if (!current || (current && current.paused && current.readyState === 4)) {
      return false;
    }
    current.pause();
  };


  const formattingTime = function(t) {
    let NowTimeValue = t,
      nowM = parseInt((NowTimeValue % 3600 / 60) + ""),
      nowS = parseInt((NowTimeValue % 60) + "");

    if (nowM < 10) {
      nowM = "0" + nowM;
    }

    if (nowS < 10) {
      nowS = "0" + nowS;
    }

    return nowM + ":" + nowS;
  };


  return (
    <div
      className={`flex ${videoBoxStyle}`}
      onMouseEnter={videoPlay}
      onMouseMove={videoPlay}
      onMouseLeave={videoPause}
    >
      <video src={url} ref={videoDOM} muted loop/>
      <div className={`${videoTimeStyle}`}>{formattingTime(duration)}</div>
    </div>
  );

}

export default VideoComponent;

