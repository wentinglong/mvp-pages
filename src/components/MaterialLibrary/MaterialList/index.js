import React from "react";
import { message } from "antd";
import { connect } from "dva";
import { Scrollbars } from "react-custom-scrollbars";
import styles from "./index.less";
import VideoComponent from "./VideoComponent";


function MaterialList(props) {

  const { editor, dispatch, materialList } = props,
    scrollStyle = {
      width: "100%",
      height: "100%",
    },
    iconCode = {
      __html: editor.isOpenMaterialList ? "&#xe681;" : "&#xe67e;",
    },
    renderProps = {
      materialListContainerStatus: editor.isOpenMaterialList ? styles.packUp : styles.packOpen,
    },
    { materialList: list } = materialList;


  const handleActionClick = () => {
    dispatch({
      type: "editor/save",
      payload: {
        isOpenMaterialList: !editor.isOpenMaterialList,
      },
    });
  };

  const materialClick = (item) => {
    const {
      currentGroup, currentMaterialIndex, isShowCurrentTransform, assetList, allMaterialByGroup,
    } = props["editor"];
    if (!isShowCurrentTransform) {
      message.info("请选择需要替换的素材对象");
      return;
    }
    for (const groupKey in currentGroup) {
      if (currentGroup.hasOwnProperty(groupKey)) {
        let materialList = currentGroup[groupKey];
        materialList.map((asset, i) => {
          if (asset["key"] === currentMaterialIndex) {
            let newUrl = item["url"],
              isVideo = false;

            switch (item["source_type"]) {
              case "mp4":
                if (newUrl.indexOf("static.atvideo.cc") !== -1) {
                  newUrl += "?vframe/png/offset/0";
                } else {
                  newUrl += "?x-oss-process=video/snapshot,t_0";
                }
                isVideo = true;
                break;
              default:
                newUrl = item["url"];
                break;
            }

            dispatch({
              type: "editor/replaceImage",
              payload: {
                material: item,
                url: newUrl,
                assetName: asset["assetName"],
                assetList: assetList,
                groupKey: groupKey,
                allMaterialByGroup: allMaterialByGroup,
                isVideo: isVideo,
                i: i,
              },
            });
          }
          return null;
        });

      }
    }


  };

  const renderMaterialList = (dataList) => {
    const renderLi = (item) => {
      const { id, url } = item;
      let node = null;

      switch (item["source_type"]) {
        case "jpg":
        case "png":
          node = <img src={url} alt="materialImg"/>;
          break;

        case "mp4":
          node = (
            <VideoComponent
              url={url}
              videoTimeStyle={styles.videoTime}
              videoBoxStyle={styles.videoBox}
            />
          );
          break;

        default:
          node = null;
          break;
      }

      return (
        <li
          key={id}
          className={`flex`}
          onClick={() => materialClick(item)}
        >
          {node}
        </li>
      );

    };


    return dataList.map(listItem => renderLi(listItem));
  };


  return (
    <div
      className={`borderBox ${styles.materialListContainer} ${renderProps.materialListContainerStatus}`}
    >
      <div
        className={`flex cursorPointer ${styles.actionIconContainer}`}
        onClick={handleActionClick}
      >
        <span className={`iconfont  ${styles.actionIcon}`} dangerouslySetInnerHTML={iconCode}/>
      </div>
      <Scrollbars style={scrollStyle}>
        <ul className={`flex ${styles.materialList}`}>
          {renderMaterialList(list)}
        </ul>
      </Scrollbars>
    </div>
  );

}

const stateToProps = (state) => {
  return {
    editor: state["editor"],
    materialList: state["materialList"],
    loading: state["loading"],
  };
};

export default connect(stateToProps)(MaterialList);
