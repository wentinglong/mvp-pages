import React from "react";
import { connect } from "dva";
import styles from "./index.less";
import Help from "./Help";
import MaterialList from "./MaterialList";


function MaterialLibrary(props) {

  const { materialLibrarySelectType } = props["materialList"],
    { dispatch } = props;


  const tagList = [
    {
      id: 1,
      title: "图片",
      fontCode: "&#xe686;",
    },
    {
      id: 2,
      title: "视频",
      fontCode: "&#xe690;",
    },
  ];
  //标签点击事件
  const handleTagOptionClick = (id) => {
    dispatch({
      type: "materialList/fetch",
      payload: {
        asset_type: id,
      },
    });
    dispatch({
      type: "editor/save",
      payload: {
        isOpenMaterialList: true,
      },
    });
  };


  const renderTagList = (list) => {
    const renderLi = (key, title, fontCode) => {
      let activeClass = key === materialLibrarySelectType ? styles.tagOptionActive : "";
      return (
        <li
          key={key}
          className={`flex cursorPointer ${styles.tagOption} ${activeClass}`}
          onClick={() => handleTagOptionClick(key)}
        >
          <span className={`iconfont ${styles.materialIcon}`} dangerouslySetInnerHTML={{ __html: fontCode }}/>
          <span className={`${styles.materialTitle}`}>{title}</span>
        </li>
      );
    };

    return list.map(tagItem => renderLi(tagItem.id, tagItem.title, tagItem.fontCode));
  };


  return (
    <div className={`borderBox ${styles.materialLibraryContainer}`}>
      <div className={`${styles.materialZIndexChildContainer}`}>
        {/* 标签列表 */}
        <ul className={`flex ${styles.tagList}`}>
          <li className={`flex ${styles.tagOption}`}>
            <span className={`iconfont ${styles.materialIcon}`}>&#xe68c;</span>
            <span className={`${styles.materialTitle}`}>素材库</span>
          </li>
          {renderTagList(tagList)}
        </ul>
        {/* 帮助 */}
        <Help/>
      </div>
      {/* 素材列表 */}
      <MaterialList/>

    </div>
  );
}


const stateToProps = (state) => {
  return {
    materialList: state["materialList"],
    loading: state["loading"],
  };
};

export default connect(stateToProps)(MaterialLibrary);
