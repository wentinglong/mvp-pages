import React, { PureComponent } from "react";
import { Slider } from "antd";
import { connect } from "dva";
import styles from "./index.less";

class EditGraphic extends PureComponent {

  constructor(props) {
    super(props);
    this.updateId = -1;
  }

  getImageUrl() {
    const { currentGroup, currentMaterialIndex, assetList } = this.props["editor"];
    for (const key in currentGroup) {
      if (currentGroup.hasOwnProperty(key)) {
        let list = currentGroup[key],
          i = 0,
          len = list.length;
        for (; i < len; i++) {
          let material = list[i];
          if (material["key"] === currentMaterialIndex) {
            return {
              imageUrl: assetList[material["assetName"]],
              material: material,
            };
          }

        }
      }
    }

    return {
      imageUrl: "",
      material: {},
    };

  }

  changeVolume = (e, material) => {
    clearTimeout(this.updateId);
    this.updateId = setTimeout(() => {
      const { dispatch } = this.props;
      clearTimeout(this.updateId);
      dispatch({
        type: "editor/changeGroupData",
        payload: {
          type: "changeVolume",
          data: {
            volume: e,
          },
        },
      });
    }, 100);
  };

  renderVolumeSlider(isShow, material) {
    if (!isShow) return null;
    const volume = material.volume || 100;

    return (
      <>
        <p className={`borderBox ${styles.groupTitle}`}>上传视频控制</p>
        <div style={{ height: 28 }}/>
        <div className={`flex ${styles.volume}`}>
          <span className={`iconfont ${styles.volumeIcon}`}>&#xe693;</span>
          <div className={` ${styles.volumeSlider}`}>
            <Slider defaultValue={volume} max={100} onChange={(e) => this.changeVolume(e, material)}/>
          </div>
        </div>
        <div style={{ height: 32 }}/>
        <div className={styles.editGraphicGroupLine}/>
        <div style={{ height: 24 }}/>
        <div className={`flex`}>
          <button onClick={this.showVideoClipBox} className={`flex cursorPointer ${styles.button}`}>
            <span className={`iconfont ${styles.uploadIcon}`}>&#xe697;</span>
            裁切视频
          </button>
        </div>
      </>
    );
  }

  renderCorpImageBox(isShow) {
    if (isShow) {
      return null;
    }

    return (
      <>
        <p className={`borderBox ${styles.groupTitle}`}>上传图片控制</p>
        <div style={{ height: 38 }}/>
        <div className={`flex`}>
          <button onClick={this.showCorpBox} className={`flex cursorPointer ${styles.button}`}>
            <span className={`iconfont ${styles.uploadIcon}`}>&#xe697;</span>
            裁切图片
          </button>
        </div>
      </>
    );
  }

  showMaterialModel = () => {
    const { dispatch } = this.props;

    dispatch({
      type: "editor/save",
      payload: {
        isShowMaterialListModel: true,
      },
    });
  };

  showCorpBox = () => {
    const { dispatch } = this.props;
    dispatch({
      type: "editor/save",
      payload: {
        isShowCorpImage: true,
      },
    });
  };

  showVideoClipBox = () => {
    const { dispatch } = this.props;
    dispatch({
      type: "editor/save",
      payload: {
        isShowVideoClip: true,
      },
    });
  };

  resetTransform = () => {
    const { dispatch, editor } = this.props;
    dispatch({
      type: "editor/changeGroupData",
      payload: {
        type: "reset",
      },
    });

    dispatch({
      type: "editor/resetCurrentMaterialTransform",
      payload: {
        allMaterialByGroup: editor.allMaterialByGroup,
        allOriginMaterialByGroup: editor.allOriginMaterialByGroup,
        currentMaterialIndex: editor.currentMaterialIndex,
        currentGroupIndex: editor.currentGroupIndex,
      },
    });
  };

  renderLocalUploadFile = () => {
    const { editor } = this.props;
    if (editor.isVeLeapTest) return null;
    return (
      <div className={`flex`}>
        <button onClick={this.showMaterialModel} className={`flex cursorPointer ${styles.button}`}>
          <span className={`iconfont ${styles.uploadIcon}`}>&#xe697;</span>
          更换图片/视频
        </button>
      </div>
    );
  };


  render() {
    const { material, imageUrl } = this.getImageUrl();

    return (
      <div className={styles.editGraphicContainer}>
        <div className={`flex borderBox ${styles.imageBox}`}>
          <img src={imageUrl} alt="asset"/>
        </div>
        {this.renderLocalUploadFile()}
        <div style={{ height: 32 }}/>
        <div className={styles.editGraphicGroupLine}/>
        <div style={{ height: 24 }}/>
        {this.renderVolumeSlider(material["isVideo"], material)}
        {this.renderCorpImageBox(material["isVideo"])}
        <div style={{ height: 24 }}/>
        <div className={`flex`}>
          <button onClick={this.resetTransform} className={`flex cursorPointer ${styles.button}`}>
            重置位置
          </button>
        </div>

      </div>
    );
  }
}


const stateToProps = (state) => {
  return {
    editor: state["editor"],
    loading: state["loading"],
  };
};

export default connect(stateToProps)(EditGraphic);
