import React, { PureComponent } from "react";
import { connect } from "dva";
import styles from "./index.less";
import { Scrollbars } from "react-custom-scrollbars";
import EditFont from "./EditFont";
import EditGraphic from "./EditGraphic";
import EditGroups from "./EditGroups";
import backIcon from "assets/img/backIcon.svg";

class EditWindow extends PureComponent {

  handleLayerClick = () => {
    console.log("显示图层!");
  };

  renderCurrentWindow() {
    const { editWindowCurrentIndex } = this.props["editor"];
    let renderNode = null;

    switch (editWindowCurrentIndex) {
      case 1:
        renderNode = <EditGroups/>;
        break;

      case 2:
        renderNode = <EditGraphic/>;
        break;

      case 3:
        renderNode = <EditFont/>;
        break;

      default:
        renderNode = null;
        break;
    }

    return renderNode;
  }

  renderEditWindowTitle() {
    const { editWindowCurrentIndex } = this.props["editor"];
    let renderNode = null;

    switch (editWindowCurrentIndex) {
      case 1:
        renderNode = "编辑组";
        break;

      case 2:
        renderNode = "上传视频/图片 ";
        break;

      case 3:
        renderNode = "文本";
        break;

      default:
        renderNode = "编辑";
        break;
    }

    return renderNode;
  }

  changeEditWindowIndex = () => {
    const { dispatch } = this.props;

    dispatch({
      type: "editor/save",
      payload: {
        editWindowCurrentIndex: 1,
      },
    });
  };

  renderBackIcon = () => {
    const { editWindowCurrentIndex } = this.props["editor"];
    let renderNode = null;

    switch (editWindowCurrentIndex) {

      case 2:
      case 3:
        renderNode = (
          <img
            className={styles.backIcon}
            onClick={this.changeEditWindowIndex}
            src={backIcon}
            alt="backIcon"
          />
        );
        break;

      default:
        renderNode = null;
        break;
    }

    return renderNode;
  };

  render() {
    const { editor: { editWindowWithPosition } } = this.props;

    const renderProps = {
      editWindowWith: editWindowWithPosition ? styles.withRight : styles.withLeft,
    };


    return (
      <div className={`flex ${styles.editWindowContainer} ${renderProps.editWindowWith}`}>
        <div className={`borderBox ${styles.editWindowLeft}`}>
          <ul className={`flex ${styles.editWindowIconList}`}>
            {/*<li*/}
            {/*  className={`flex borderBox iconfont cursorPointer`}*/}
            {/*  onClick={this.handleLayerClick}*/}
            {/*>*/}
            {/*  &#xe68d;*/}
            {/*</li>*/}
          </ul>
        </div>

        <div className={`flex borderBox ${styles.editWindowRight}`}>
          <h4 className={`flex ${styles.editWindowTitle}`}>
            {this.renderEditWindowTitle()}
            {this.renderBackIcon()}
          </h4>
          <div className={styles.editWindowActionContainer}>
            <Scrollbars>
              {this.renderCurrentWindow()}
            </Scrollbars>
          </div>
        </div>
      </div>
    );
  }


}


const stateToProps = (state) => {
  return {
    editor: state["editor"],
    loading: state["loading"],
  };
};

export default connect(stateToProps)(EditWindow);
