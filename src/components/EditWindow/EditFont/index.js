import React, { PureComponent } from "react";
import { connect } from "dva";
import styles from "./index.less";
import Color from "./Color";
import Style from "./Style";


class EditFont extends PureComponent {

  constructor(props) {
    super(props);
    this.state = {
      currentTab: 1,
    };
  }

  changeTab = (id) => {
    this.setState({
      currentTab: id,
    });
  };

  renderTabLi() {
    const list = [
        {
          id: 1,
          text: "样式",
        },
        {
          id: 2,
          text: "颜色",
        },
      ],
      { currentTab } = this.state;


    return list.map(tab => {
      const { id, text } = tab,
        isActive = currentTab === id ? styles.active : "";

      return (
        <li
          key={id}
          className={`flex cursorPointer ${isActive}`}
          onClick={() => this.changeTab(id)}
        >
          {text}
        </li>
      );
    });

  }

  renderTabContent() {
    let renderNode = null,
      { currentTab } = this.state;

    switch (currentTab) {
      case 1:
        renderNode = <Style/>;
        break;

      case 2:
        renderNode = <Color/>;
        break;

      default:
        renderNode = null;
        break;
    }

    return renderNode;

  }

  render() {
    return (
      <div className={`${styles.editFontContainer}`}>
        <ul className={`flex ${styles.tabList}`}>
          {this.renderTabLi()}
        </ul>
        <div className={`${styles.tabContainer}`}>
          {this.renderTabContent()}
        </div>

      </div>
    );
  }

}


const stateToProps = (state) => {
  return {
    editor: state["editor"],
    loading: state["loading"],
  };
};

export default connect(stateToProps)(EditFont);
