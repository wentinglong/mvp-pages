import React, { PureComponent } from "react";
import styles from "./index.less";
import { ChromePicker } from "react-color";
import { connect } from "dva";
import { Slider } from "antd";

class Color extends PureComponent {

  constructor(props) {
    super(props);
    this.state = {
      isShowColorSelect: false,
      selectIndex: -1,
    };
  }

  componentDidMount() {
    document.addEventListener("click", this.closeSelectColor);
  }

  componentWillUnmount() {
    document.removeEventListener("click", this.closeSelectColor);
  }

  closeSelectColor = () => {
    this.setState({
      isShowColorSelect: false,
    });
  };

  openSelectColor = (e, i) => {
    e.nativeEvent.stopImmediatePropagation();
    this.setState({
      isShowColorSelect: true,
      selectIndex: i,
    });
  };

  selectColorChange = (e) => {
    const { dispatch } = this.props,
      { selectIndex } = this.state;
    dispatch({
      type: "editor/changeGroupData",
      payload: {
        type: selectIndex === 1 ? "changeTextColor" : "changeStrokeColor",
        data: {
          color: e.hex,
        },
      },
    });
  };

  renderSelectColor(material) {
    const { isShowColorSelect, selectIndex } = this.state;
    if (!isShowColorSelect) return null;
    const { fontSetting } = material;

    let textColor = "",
      strokeColor = "",
      color = "";
    // strokeWidth
    if (fontSetting) {
      textColor = fontSetting.textColor;
      strokeColor = fontSetting.strokeColor;
    }

    if (selectIndex === 1) {
      color = textColor;
    } else {
      color = strokeColor;
    }


    const handleClick = (e) => {
      e.nativeEvent.stopImmediatePropagation();
    };


    return (
      <div className={`${styles.colorSelectBox}`} onClick={handleClick}>
        <ChromePicker
          color={color}
          onChange={(e) => this.selectColorChange(e)}
          disableAlpha={true}
        />
      </div>
    );
  }

  getMaterial() {
    const { currentGroup, currentMaterialIndex } = this.props["editor"];
    for (const key in currentGroup) {
      if (currentGroup.hasOwnProperty(key)) {
        let list = currentGroup[key],
          i = 0,
          len = list.length;
        for (; i < len; i++) {
          let material = list[i];
          if (material["key"] === currentMaterialIndex) {
            return material;
          }

        }
      }
    }

    return {};
  }

  changeWidth(e) {
    const { dispatch } = this.props;
    dispatch({
      type: "editor/changeGroupData",
      payload: {
        type: "changeStrokeWidth",
        data: {
          width: e,
        },
      },
    });
  }


  render() {

    const material = this.getMaterial(),
      { fontSetting } = material;

    let textColor = "",
      strokeColor = "",
      strokeWidth = 0;
    if (fontSetting) {
      textColor = fontSetting.textColor;
      strokeColor = fontSetting.strokeColor;
      strokeWidth = fontSetting.strokeWidth;
    }


    return (
      <>
        <p className={`flex ${styles.colorTitle}`}>文字颜色</p>
        <div
          className={`${styles.colorContent} ${styles.border}`}
          onClick={(e) => this.openSelectColor(e, 1)}
          style={{
            backgroundColor: textColor,
          }}
        />

        <p className={`flex ${styles.colorTitle}`}>描边颜色</p>
        <div
          className={`${styles.colorContent} ${styles.border}`}
          onClick={(e) => this.openSelectColor(e, 2)}
          style={{
            backgroundColor: strokeColor,
          }}
        />

        <p className={`flex ${styles.colorTitle}`}>描边粗细</p>
        <div className={`flex ${styles.colorContent}`}>
          <div className={` ${styles.volumeSlider}`}>
            <Slider defaultValue={strokeWidth} max={100} onChange={(e) => this.changeWidth(e)}/>
          </div>
        </div>


        {this.renderSelectColor(material)}
      </>
    );
  }


}

const stateToProps = (state) => {
  return {
    editor: state["editor"],
    loading: state["loading"],
  };
};


export default connect(stateToProps)(Color);
