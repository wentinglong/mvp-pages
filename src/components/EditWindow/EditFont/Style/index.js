import React from "react";
import styles from "./index.less";
import { connect } from "dva";

function Style(props) {

  const getMaterial = () => {
    const { currentGroup, currentMaterialIndex } = props["editor"];
    for (const key in currentGroup) {
      if (currentGroup.hasOwnProperty(key)) {
        let list = currentGroup[key],
          i = 0,
          len = list.length;
        for (; i < len; i++) {
          let material = list[i];
          if (material["key"] === currentMaterialIndex) {
            return material;
          }

        }
      }
    }
    return {};
  };

  const { fontSetting } = getMaterial();

  let fontSize = 0;

  if (fontSetting) {
    fontSize = fontSetting.fontSize;
  }

  const setFontSize = (val) => {
    const { dispatch } = props;
    if (/^[0-9]*$/.test(val)) {
      val = val < 12 ? 12 : val;
      dispatch({
        type: "editor/changeGroupData",
        payload: {
          type: "changeFontSize",
          data: {
            size: Number(val),
          },
        },
      });
    }


  };


  return (
    <>
      <div className={`flex ${styles.fontSizeContainer}`}>
        <div className={`flex borderBox ${styles.fontSizeInput}`}>
          <input
            value={fontSize}
            onChange={(e) => setFontSize(e.target.value)}
          />
          <span>px</span>
        </div>
        <div className={`flex borderBox ${styles.fontSizeBtn}`}>
          <button
            className={`borderBox cursorPointer`}
            onClick={() => setFontSize(fontSize + 1)}
          >
            A+
          </button>
          <button
            onClick={() => setFontSize(fontSize - 1)}
            className={`borderBox cursorPointer`}
          >
            A-
          </button>
        </div>
      </div>
    </>
  );
}

const stateToProps = (state) => {
  return {
    editor: state["editor"],
    loading: state["loading"],
  };
};


export default connect(stateToProps)(Style);
