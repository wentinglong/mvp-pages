import React, { PureComponent } from "react";
import { connect } from "dva";
import styles from "./index.less";

class EditGroups extends PureComponent {

  handleImageMaterial = (item, isShow) => {
    const { dispatch } = this.props;
    let editWindowCurrentIndex = 1;
    if (isShow) {
      editWindowCurrentIndex = 2;
    } else {
      editWindowCurrentIndex = 3;
    }

    dispatch({
      type: "editor/save",
      payload: {
        isShowCurrentTransform: isShow,
        currentMaterialIndex: item["key"],
        isOpenMaterialList: isShow,
        editWindowCurrentIndex: editWindowCurrentIndex,
        isFocusText: true,
      },
    });

  };

  renderGroups() {
    const { currentGroup, assetList } = this.props["editor"];
    let materialList = [];
    for (const currentGroupKey in currentGroup) {
      if (currentGroup.hasOwnProperty(currentGroupKey)) {
        materialList = currentGroup[currentGroupKey];
      }
    }

    return materialList.map(item => {
      const { key, assetName, type, fontSetting } = item;
      let node = null;
      switch (type) {
        case 1:
          node = (
            <li
              key={key}
              className={`flex borderBox cursorPointer`}
              onClick={() => this.handleImageMaterial(item, true)}
            >
              <img src={assetList[assetName]} alt="assetImg"/>
            </li>
          );
          break;

        case 2:
        case 3:
          node = (
            <li
              key={key}
              className={`flex borderBox cursorPointer`}
              onClick={() => this.handleImageMaterial(item, false)}
            >
              {fontSetting.defaultText}
            </li>
          );
          break;

        default:
          node = null;
          break;
      }

      return node;
    });
  }

  render() {
    return (
      <ul className={`flex borderBox ${styles.editGroupsContainer}`}>
        {this.renderGroups()}
      </ul>
    );
  }

}


const stateToProps = (state) => {
  return {
    editor: state["editor"],
    loading: state["loading"],
  };
};

export default connect(stateToProps)(EditGroups);
