import React, { PureComponent } from "react";
import Moveable from "react-moveable";


class Transform extends PureComponent {

  componentDidMount() {

  }


  onDragStart = (dragStartProps) => {
    const { handleDragStart } = this.props;
    handleDragStart && handleDragStart(dragStartProps);
  };

  onDrag = (dragProps) => {
    dragProps.target.style.transform = dragProps.transform;
    const { handleDrag } = this.props;
    handleDrag && handleDrag(dragProps);
  };

  onDragEnd = (dragEndProps) => {
    const { handleDragEnd } = this.props;
    handleDragEnd && handleDragEnd(dragEndProps);
  };

  onScaleStart = (scaleStartProps) => {
    const { handleScaleStart } = this.props;
    handleScaleStart && handleScaleStart(scaleStartProps);
  };

  onScale = (scaleProps) => {
    scaleProps.target.style.transform = scaleProps.transform;
    const { handleScale } = this.props;
    handleScale && handleScale(scaleProps);

  };

  onScaleEnd = (scaleEndProps) => {
    const { handleScaleEnd } = this.props;
    handleScaleEnd && handleScaleEnd(scaleEndProps);
  };

  onRotateStart = (rotateStartProps) => {
    const { handleRotateStart } = this.props;
    handleRotateStart && handleRotateStart(rotateStartProps);
  };

  onRotate = (rotateProps) => {
    rotateProps.target.style.transform = rotateProps.transform;
    const { handleRotate } = this.props;
    handleRotate && handleRotate(rotateProps);
  };

  onRotateEnd = (rotateEndProps) => {
    const { handleRotateEnd } = this.props;
    handleRotateEnd && handleRotateEnd(rotateEndProps);
  };


  render() {
    const {
      onDragStart, onDrag, onDragEnd,
      onScaleStart, onScale, onScaleEnd,
      onRotateStart, onRotate, onRotateEnd,
    } = this;


    return (
      <Moveable
        target={this.props.target}
        container={null}
        origin={false}
        keepRatio={true}
        scalable={true}
        draggable={true}
        rotatable={true}
        className={"moveableCustom"}
        renderDirections={["nw", "ne", "sw", "se"]}

        onDragStart={onDragStart}
        onDrag={onDrag}
        onDragEnd={onDragEnd}

        onScaleStart={onScaleStart}
        onScale={onScale}
        onScaleEnd={onScaleEnd}

        onRotateStart={onRotateStart}
        onRotate={onRotate}
        onRotateEnd={onRotateEnd}

      />
    );
  }


}


export default Transform;
