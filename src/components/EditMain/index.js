import React, { PureComponent } from "react";
import styles from "./index.less";
import Transform from "./Transform";
import { connect } from "dva";
import Affine from "utils/lib/Affine";

class EditMain extends PureComponent {

  constructor(props) {
    super(props);
    this.state = {
      domRefList: {},
      canvasDom: null,
    };
  }

  renderTransform() {
    const { isShowCurrentTransform, currentMaterialIndex } = this.props["editor"];
    if (!isShowCurrentTransform) return null;
    const { handleDragEnd, handleScaleEnd, handleRotateEnd, handleTransformStart } = this,
      { domRefList } = this.state;
    return (
      <Transform
        target={domRefList[currentMaterialIndex]}
        handleScaleEnd={handleScaleEnd}
        handleDragEnd={handleDragEnd}
        handleRotateEnd={handleRotateEnd}
        handleScaleStart={handleTransformStart}
        handleDragStart={handleTransformStart}
        handleRotateStart={handleTransformStart}
      />
    );
  }

  handleTransformStart = () => {
    const { dispatch } = this.props;
    dispatch({
      type: "editor/save",
      payload: {
        isShowOriginMaterialContainer: true,
      },
    });
  };

  handleRotateEnd = ({ lastEvent }) => {
    const { dispatch } = this.props;

    dispatch({
      type: "editor/save",
      payload: {
        isShowOriginMaterialContainer: false,
      },
    });

    if (!lastEvent) return;
    dispatch({
      type: "editor/changeGroupData",
      payload: {
        type: "rotate",
        data: {
          rotate: lastEvent.rotate,
        },
      },
    });

  };

  handleScaleEnd = ({ lastEvent }) => {
    const { dispatch } = this.props;
    dispatch({
      type: "editor/save",
      payload: {
        isShowOriginMaterialContainer: false,
      },
    });

    if (!lastEvent) return;
    const offsetScaleX = lastEvent.scale[0],
      offsetScaleY = lastEvent.scale[1];

    dispatch({
      type: "editor/changeGroupData",
      payload: {
        type: "scale",
        data: {
          offsetScaleX: offsetScaleX,
          offsetScaleY: offsetScaleY,
        },
      },
    });


  };

  handleDragEnd = (position) => {
    const { dispatch } = this.props;
    dispatch({
      type: "editor/save",
      payload: {
        isShowOriginMaterialContainer: false,
      },
    });

    if (!position.lastEvent) return;
    const offsetX = position.lastEvent.beforeTranslate[0],
      offsetY = position.lastEvent.beforeTranslate[1];

    dispatch({
      type: "editor/changeGroupData",
      payload: {
        type: "position",
        data: {
          offsetX: offsetX,
          offsetY: offsetY,
        },
      },
    });


  };

  materialRef = (ref, item) => {
    this.setState(state => {
      state.domRefList[item["key"]] = ref;
      return {
        ...state,
      };
    });
  };

  handleImageMaterialClick = (item) => {
    const { dispatch } = this.props,
      key = item["key"];

    dispatch({
      type: "editor/save",
      payload: {
        isShowCurrentTransform: true,
        isOpenMaterialList: true,
        currentMaterialIndex: key,
        editWindowCurrentIndex: 2,
      },
    });
  };

  handleTextFocus = (item) => {
    const { dispatch } = this.props,
      { currentMaterialIndex } = this.props["editor"],
      key = item["key"];


    if (currentMaterialIndex === key) {
      return false;
    }

    dispatch({
      type: "editor/save",
      payload: {
        isShowCurrentTransform: false,
        currentMaterialIndex: key,
        editWindowCurrentIndex: 3,
        isOpenMaterialList: false,
      },
    });
  };

  handleTextBlur = () => {
    const { isFocusText } = this.props["editor"],
      { dispatch } = this.props;
    if (isFocusText) {
      dispatch({
        type: "editor/save",
        payload: {
          isFocusText: false,
        },
      });
    }
  };

  handleTextChange = (e) => {
    const newVal = e.target.value,
      { dispatch } = this.props;

    dispatch({
      type: "editor/changeGroupData",
      payload: {
        type: "changeText",
        data: {
          text: newVal,
        },
      },
    });


  };

  renderCanvasMask = (showStyleInfo) => {
    const { isShowOriginMaterialContainer } = this.props["editor"],
      {
        editor: { currentMaterialIndex, allOriginMaterialByGroup, currentGroupIndex },
        scale,
      } = this.props,
      currentGroup = allOriginMaterialByGroup[currentGroupIndex] || {},
      keys = Object.keys(currentGroup),
      { canvasDom } = this.state;
    if (keys.length <= 0) return null;
    currentGroup.map(item => {
      if (item["key"] !== currentMaterialIndex) return null;
      if (isShowOriginMaterialContainer) return null;
      if (!canvasDom) return null;
      const context = canvasDom.getContext("2d"),
        { containerWidth, containerHeight } = showStyleInfo;

      let s = item.scale[0],
        x = item.position[0],
        y = item.position[1],
        aX = item.anchor[0],
        aY = item.anchor[1],
        w = item["originSize"][0],
        h = item["originSize"][1],
        r = item.rotate,
        matrix = new Affine(),
        anchor = { x: aX, y: aY },
        position = { x: x, y: y },
        zoom = { x: s, y: s };

      context.save();
      matrix.set(anchor, position, zoom, r);
      context.clearRect(0, 0, containerWidth / scale, containerHeight / scale);
      context.fillStyle = "rgba(0,0,0, 0.7)";
      context.fillRect(0, 0, containerWidth / scale, containerHeight / scale);
      context.globalCompositeOperation = "destination-out";
      context.setTransform(matrix.mA, matrix.mB, matrix.mC, matrix.mD, matrix.mE, matrix.mF);
      context.fillStyle = "rgba(255,255,255)";
      context.fillRect(0, 0, w, h);
      context.resetTransform();
      context.restore();
      return null;
    });


  };

  renderMaterial() {
    const {
      editor: { currentGroup, assetList, currentMaterialIndex },
      scale,
    } = this.props;

    const keys = Object.keys(currentGroup);
    if (keys.length <= 0) return null;
    const material = currentGroup[keys[0]];
    return material.map(item => {

      let uiResult = null,
        defaultZIndex = 2,
        key = item.assetName,
        s = item.scale[0],
        x = item.position[0],
        y = item.position[1],
        aX = item.anchor[0],
        aY = item.anchor[1],
        w = item["originSize"][0],
        h = item["originSize"][1],
        r = item.rotate;

      const innerStyle = {
          position: "absolute",
          left: `${-aX}px`,
          top: `${-aY}px`,
          width: `${w}px`,
          height: `${h}px`,
          transformOrigin: `${aX}px ${aY}px`,
          transform: `translate(${x}px, ${y}px) rotate(${r}deg) scale(${s})`,
        },
        containerStyle = {
          zIndex: item.zIndex + defaultZIndex,
          left: 0,
          top: 0,
          transform: `scale(${scale})`,
          opacity: `${item.transparence}`,
        };


      switch (item.type) {
        case 1:
          if (currentMaterialIndex !== item["key"]) {
            innerStyle.border = `${2 / s / scale}px dashed #FF5172`;
          }

          uiResult = (
            <div
              className={`cursorPointer ${styles.material}`}
              style={containerStyle}
              key={key}
            >
              <img
                ref={(ref) => this.materialRef(ref, item)}
                onClick={() => this.handleImageMaterialClick(item)}
                style={innerStyle}
                src={assetList[key]}
                alt="asset"
              />
            </div>
          );
          break;
        case 2:
        case 3:
          const {
              defaultText, fontSize, textAlign, textColor, maxLength, strokeWidth, strokeColor,
            } = item.fontSetting,
            textAligns = {
              0: "left",
              1: "right",
              2: "center",
            },
            textareaStyle = {
              ...innerStyle,
              textAlign: textAligns[textAlign],
              fontSize: `${fontSize}px`,
              color: textColor,
              borderWidth: `${2 / s / scale}px`,
              WebkitTextStroke: `${strokeWidth}px ${strokeColor}`,
            };
          textareaStyle.fontSize = fontSize;
          containerStyle.zIndex += 30;
          uiResult = (
            <div
              className={styles.material}
              key={key}
              style={containerStyle}
            >
              <textarea
                className={styles.textarea}
                style={textareaStyle}
                maxLength={maxLength}
                defaultValue={defaultText}
                ref={(ref) => this.materialRef(ref, item)}
                onFocus={() => this.handleTextFocus(item)}
                onBlur={() => this.handleTextBlur()}
                onChange={(e) => this.handleTextChange(e, item)}
              />
            </div>
          );
          break;

        default:
          uiResult = null;
          break;
      }

      return uiResult;
    });

  }

  renderAccessory(className, image) {
    if (!image) return null;

    return (
      <div
        className={`${styles.accessoryImage} ${className}`}
        style={{
          backgroundImage: `url(${image})`,
        }}
      />
    );
  };

  textMaterialFocus() {
    const { currentMaterialIndex, isFocusText } = this.props["editor"],
      { domRefList } = this.state,
      currentText = domRefList[currentMaterialIndex];
    if (isFocusText) {
      currentText && currentText.focus();
    }
  }

  render() {
    const {
      containerHeight, isHorizontalVersion, backgroundImage, prospectImage, scale,
      editor: { showStyleInfo, isShowOriginMaterialContainer },
    } = this.props;

    this.renderCanvasMask(showStyleInfo);

    const containerStyle = {
      height: `${containerHeight}px`,
      width: `${showStyleInfo.containerWidth}px`,
    };

    return (
      <div className={`flex ${styles.editMainContainer}`}>
        <div
          className={`${styles.editorLayout} ${isHorizontalVersion ? styles.horizontalVersion : ""}`}
          style={containerStyle}
        >
          {this.renderMaterial()}
          {this.renderTransform()}
          {this.textMaterialFocus()}
          {this.renderAccessory(styles.backgroundImage, backgroundImage)}
          {this.renderAccessory(styles.prospectImage, prospectImage)}
          <canvas
            ref={(ref) => this.setState({ canvasDom: ref })}
            width={showStyleInfo["containerWidth"] / scale}
            height={showStyleInfo["containerHeight"] / scale}
            className={`${styles.renderMaterialMask} ${isShowOriginMaterialContainer ? "" : "dnHidden"}`}
          />
        </div>
        <div
          style={containerStyle}
          className={`${styles.editorLayoutEmpty}`}
        />
      </div>
    );
  }

}

const stateToProps = (state) => ({
  editor: state["editor"],
});

export default connect(stateToProps)(EditMain);


