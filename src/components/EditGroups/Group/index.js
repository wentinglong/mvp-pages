import React from "react";
import styles from "./index.less";
import { connect } from "dva";

function Group(props) {

  const {
    horizontalVersion, containerHeight, groupIndex, backgroundImage, prospectImage, containerWidth,
    assetList, allMaterial, scale, active, dispatch, parseManage,
  } = props;
  
  let { windowHeight, showStyleInfo, scale: containerScale, windowWidth } = props["editor"];

  const fullClassName = `${horizontalVersion ? styles.horizontalVersion : ""}`,
    activeClassName = `${active ? styles.active : ""}`,
    renderProps = {
      className: `flex cursorPointer borderBox ${styles.groupContainer} ${fullClassName} ${activeClassName}`,
      style: {
        height: `${containerHeight}px`,
        width: `${containerWidth}px`,
        flex: `0 0 ${containerWidth}px`,
      },
      onClick() {
        windowHeight = window.innerHeight - 300;
        windowWidth = window.innerWidth - 720;
        let container = parseManage.getWidthScaleByGroup(windowHeight, groupIndex);

        if (windowWidth <= container.width) {
          container = parseManage.getHeightScaleByGroup(windowWidth, groupIndex);
          windowHeight = container.height;
          container = parseManage.getWidthScaleByGroup(container.height, groupIndex);
        }

        dispatch({
          type: "editor/save",
          payload: {
            currentGroup: {
              [groupIndex]: allMaterial,
            },
            showStyleInfo: {
              ...showStyleInfo,
              containerHeight: windowHeight,
              containerWidth: container.width,
            },
            scale: {
              ...containerScale,
              container: container.scale,
            },
            windowHeight: windowHeight,
            currentGroupIndex: groupIndex,
            isShowCurrentTransform: false,
            currentMaterialIndex: -1,
            editWindowWithPosition: true,
            editWindowCurrentIndex: 1,
          },
        });


      },
    };

  const renderAccessory = (className, image) => {
    if (!image) return null;
    return (
      <div
        className={`${styles.accessoryImage} ${className}`}
        style={{
          backgroundImage: `url(${image})`,
        }}
      />
    );
  };

  const renderMaterial = () => {
    return allMaterial.map(material => {

      let uiResult = null,
        defaultZIndex = 2,
        s = material.scale[0],
        x = material.position[0],
        y = material.position[1],
        aX = material.anchor[0],
        aY = material.anchor[1],
        w = material["originSize"][0],
        h = material["originSize"][1],
        r = material.rotate;

      const style = {
          position: "absolute",
          left: `${-aX}px`,
          top: `${-aY}px`,
          width: `${w}px`,
          height: `${h}px`,
          opacity: `${material.transparence}`,
        },
        key = material.assetName,
        containerStyle = {
          width: "1080px",
          height: "1920px",
          left: 0,
          top: 0,
          transform: `scale(${scale})`,
          transformOrigin: `left top`,
          zIndex: material.zIndex + defaultZIndex,
        };

      switch (material.type) {
        case 1:
          style.transformOrigin = `${aX}px ${aY}px`;
          style.transform = `translate(${x}px, ${y}px) rotate(${r}deg) scale(${s})`;
          uiResult = (
            <div
              key={key}
              className={styles.material}
              style={containerStyle}
            >
              <img
                style={style}
                src={assetList[key]}
                alt="asset"
              />
            </div>
          );
          break;
        case 2:
        case 3:
          // const { defaultText, fontSize } = material.fontSetting;
          // style.fontSize = `${fontSize}px`;
          // style.transform = `translate(-50%, -50%) rotate(${material.rotate}deg) scale(${s})`;

          // uiResult = (
          //   <div
          //     className={styles.material}
          //     key={key}
          //     style={style}
          //   >
          //    <span style={{
          //      display: "flex",
          //      "transform-origin": "top left",
          //      transform: `scale(${s})`,
          //    }}>
          //      {defaultText}
          //    </span>
          //   </div>
          // );
          break;

        default:
          uiResult = null;
          break;
      }

      return uiResult;
    });

  };


  return (
    <div {...renderProps}>
      <div className={`${styles.borderWarp}`}/>
      <div className={`flex ${styles.groupIndexNum}`}>{allMaterial.length}</div>
      <div className={`flex ${styles.groupTitle}`}>{`编辑组${groupIndex}`}</div>
      {renderAccessory(styles.backgroundImage, backgroundImage)}
      {renderAccessory(styles.prospectImage, prospectImage)}
      {renderMaterial()}
    </div>
  );


}

const stateToProps = (state) => ({
  editor: state["editor"],
});

export default connect(stateToProps)(Group);
