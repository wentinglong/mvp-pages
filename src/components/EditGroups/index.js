import React, { PureComponent } from "react";
import { Scrollbars } from "react-custom-scrollbars";
import styles from "./index.less";
import Group from "./Group";
import playIcon from "assets/img/playIcon.svg";
import { connect } from "dva";
import Cropper from "utils/lib/Cropper";
import Affine from "utils/lib/Affine";
import ReplaceableJson from "utils/lib/ReplaceableJson";
import { getImage } from "utils";
import Upload from "utils/lib/Upload";
import env from "@/env";
import Cookies from "js-cookie";
import { TOKEN_NAME, PAGE_LOADING_TYPE } from "@/types";
import { openNotification } from "utils/userPrompt";

class EditGroups extends PureComponent {

  constructor(props) {
    super(props);
    this.scrollStyle = {
      flex: "1",
    };
  }

  renderGroupList(groups) {
    let key,
      list = [];
    for (key in groups) {
      if (groups.hasOwnProperty(key)) {
        list.push(this.renderGroup({ i: key, group: groups[key] }));
      }
    }

    return list;
  }

  renderGroup(groupItem) {
    const {
        horizontalVersion, containerHeight, backgroundImageList, prospectImageList,
        assetList, allMaterialByGroup, currentGroupKey, parseManage,
      } = this.props,
      groupKey = groupItem.i,
      groupShowStyle = parseManage.getWidthScaleByGroup(containerHeight, groupKey);

    return (
      <Group
        key={groupKey}
        groupIndex={groupKey}
        allMaterial={allMaterialByGroup[groupKey]}
        containerHeight={containerHeight}
        containerWidth={groupShowStyle.width}
        horizontalVersion={horizontalVersion}
        backgroundImage={backgroundImageList[parseManage.groups[groupKey]["backgroundImage"]]}
        prospectImage={prospectImageList[parseManage.groups[groupKey]["prospectImage"]]}
        scale={groupShowStyle.scale}
        assetList={assetList}
        group={groupItem.group}
        active={currentGroupKey === groupKey}
        parseManage={parseManage}
      />
    );
  }

  mapGroups(group) {
    let upload = new Upload({
      getTokenUrl: env.getTokenUrl,
      jwt: Cookies.get(TOKEN_NAME),
      sourceHost: env.sourceHost,
    });

    const mapCb = async material => {
      let {
          type, assetName, originSize, isVideo,
          key, distRotate, distScale, distPosition, volume, videoClip,
        } = material,
        { assetList, parseManage } = this.props["editor"],
        currentUi = parseManage["uiAssets"][key]["ui"],
        editScale = currentUi["s"][0],
        materialWidth = currentUi["editSize"][0],
        materialHeight = currentUi["editSize"][1],
        originWidth = originSize[0],
        originHeight = originSize[1],
        isChangeMaterialSource = (originWidth !== materialWidth && originHeight !== materialHeight),
        cropOptions = {
          width: materialWidth,
          height: materialHeight,
        },
        result = {
          key,
          type,
        };


      if (type === 1) {
        let url = assetList[assetName],
          matrix = {
            anchor: {
              x: 0,
              y: 0,
            },
            position: {
              x: 0,
              y: 0,
            },
            zoom: {
              x: 1,
              y: 1,
            },
            rotation: 0,
          },
          centerX = materialWidth / 2,
          centerY = materialHeight / 2;

        if (distRotate || distScale || distPosition) {
          matrix.anchor = {
            x: centerX,
            y: centerY,
          };
          matrix.position = {
            x: centerX,
            y: centerY,
          };

          if (distRotate) {
            matrix.rotation = distRotate;
          }

          if (distScale) {
            matrix.zoom.x = distScale[0];
            matrix.zoom.y = distScale[1];
          }

          if (distPosition) {
            matrix.position.x += distPosition[0] / editScale;
            matrix.position.y += distPosition[1] / editScale;
          }
        }

        if (isChangeMaterialSource) {
          matrix.anchor = {
            x: originWidth / 2,
            y: originHeight / 2,
          };
          if (!distPosition) {
            matrix.position = {
              x: centerX,
              y: centerY,
            };
          }
        }

        if (isVideo) {
          result["imgUrl"] = url.substring(0, url.lastIndexOf("?"));
          result["isVideo"] = isVideo;
          result["adapt_type"] = 4;
          result["volume"] = volume;
          result["videoClip"] = videoClip;
          let videoMatrix = new Affine();
          videoMatrix.set(matrix.anchor, matrix.position, matrix.zoom, matrix.rotation);

          result["videoTransform"] = [
            videoMatrix.mA, videoMatrix.mC, videoMatrix.mE,
            videoMatrix.mB, videoMatrix.mD, videoMatrix.mF,
          ];

          return result;
        }

        let crop = new Cropper(cropOptions),
          img = await getImage(url);
        crop.setAffine(matrix);
        console.log(`${assetName} 开始截图...`);
        await crop.drawImage(img, 0, 0, originWidth, originHeight);
        console.log(`${assetName} 截图完成!`);
        let bolbData = await crop.getBlobData(),
          fileData = new File([bolbData], assetName, { type: bolbData.type });
        console.log(`${assetName} 开始上传...`);
        result["imgUrl"] = await upload.uploadFile(fileData);
        console.log(`${assetName} 上传完成`);
        console.log(result["imgUrl"]);
      }

      if (type === 2 || type === 3) {
        result["fontSetting"] = material["fontSetting"];
      }

      return result;
    };


    return Promise.all(group.map(mapCb));
  }

  async mapAllMaterialByGroup(allMaterialByGroup) {
    let key,
      currentGroup,
      resultJson = {};
    for (key in allMaterialByGroup) {
      if (allMaterialByGroup.hasOwnProperty(key)) {
        currentGroup = allMaterialByGroup[key];
        resultJson[key] = await this.mapGroups(currentGroup);
      }
    }

    return resultJson;
  }

  dispatchMiddle = (type, payload) => {
    const { dispatch } = this.props;
    dispatch({
      type: `editor/${type}`,
      payload: payload,
    });
  };

  veLeapTestCheck = () => {
    let isSuccess = true;
    const { allMaterialByGroup, allOriginMaterialByGroup, isVeLeapTest, assetList, originAssetList } = this.props["editor"];
    if (!isVeLeapTest) return true;
    let key;

    const mapGroup = (currentGroup, currentOriginGroup) => {
      const mapCallback = (group, i) => {
        let currentAssetUrl = assetList[group.assetName],
          currentOriginAssetUrl = originAssetList[currentOriginGroup[i].assetName];

        if (
          group.type === 1
          &&
          currentAssetUrl === currentOriginAssetUrl
        ) {
          isSuccess = false;
        }

        if (
          group.type === 2
          &&
          group.fontSetting.defaultText === currentOriginGroup[i].fontSetting.defaultText
        ) {
          isSuccess = false;
        }

        return null;
      };
      currentGroup.map(mapCallback);
    };

    for (key in allMaterialByGroup) {
      if (allMaterialByGroup.hasOwnProperty(key)) {
        const currentGroup = allMaterialByGroup[key],
          currentOriginGroup = allOriginMaterialByGroup[key];
        mapGroup(currentGroup, currentOriginGroup);
      }
    }

    return isSuccess;
  };

  submitRendering = () => {
    const { allMaterialByGroup, pageLoading, parseManage, currentTemplateInfo, isVeLeapTest } = this.props["editor"],
      { a, pid, v, i, test_adopt, project_id, process_id } = this.props.query,
      { dispatchMiddle } = this;

    if (!this.veLeapTestCheck()) {
      openNotification("不符合测试要求!", "未替换全部素材", "error", 10);
      return;
    }

    if (pageLoading) return;

    dispatchMiddle("save", {
      pageLoading: true,
      pageLoadingText: "正在上传文件",
      isShowCurrentTransform: false,
      currentMaterialIndex: -1,
      editWindowCurrentIndex: 1,
    });
    localStorage.setItem(PAGE_LOADING_TYPE, "1");

    this.mapAllMaterialByGroup(allMaterialByGroup).then(res => {
      dispatchMiddle("save", {
        pageLoading: false,
      });
      localStorage.setItem(PAGE_LOADING_TYPE, "0");
      const replaceJson = new ReplaceableJson(res, parseManage.orderList, currentTemplateInfo.id),
        json = replaceJson.getJson(),
        renderType = isVeLeapTest ? "veLeapTestSubmitRender" : "submitRender";

      dispatchMiddle(renderType, {
        json: json,
        veLeapToken: v,
        a,
        pid,
        i,
        test_adopt,
        project_id,
        process_id,
      });

    });

  };

  render() {

    const {
        isOpenMaterialList, groups, editorContainerHeight, totalTime,
      } = this.props,
      renderProps = {
        containerWithPosition: isOpenMaterialList ? styles.withRight : styles.withLeft,
        containerHeight: editorContainerHeight,
      },
      fullClassName = `${styles.editGroupsContainer} ${renderProps.containerWithPosition}`,
      groupList = groups || {};

    return (
      <div
        className={`flex borderBox ${styles.editGroupsContainer} ${fullClassName}`}
        style={{
          height: `${renderProps.containerHeight}px`,
        }}
      >
        <div className={`flex ${styles.playBtnBox}`}>
          <div className={`cursorPointer ${styles.playIcon}`} onClick={this.submitRendering}>
            <img src={playIcon} alt="play"/>
          </div>
          <div className={`${styles.totalTime}`}>{`总时长：${totalTime}s`}</div>
        </div>
        <Scrollbars style={this.scrollStyle}>
          <div className={`flex ${styles.editGroupsList}`}>
            {this.renderGroupList(groupList)}
          </div>
        </Scrollbars>
      </div>
    );
  }

}

const stateToProps = (state) => {
  let loading = state["loading"]["global"];
  return {
    loadingEffects: loading,
    query: state["router"]["location"]["query"],
    editor: state["editor"],
  };
};

export default connect(stateToProps)(EditGroups);

