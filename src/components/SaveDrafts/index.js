import React, { PureComponent } from "react";
import { Button } from "antd";
import { connect } from "dva";
import style from "./index.less";

class SaveDrafts extends PureComponent {

  handleSave = () => {
    const { editor, query, dispatch } = this.props,
      saveJson = {};
    saveJson.allMaterialByGroup = editor.allMaterialByGroup;
    saveJson.assetList = editor.assetList;

    dispatch({
      type: "editor/veLeapTestSave",
      payload: {
        token: query["v"],
        record_json: JSON.stringify(saveJson),
        id: query["pid"],
      },
    });
  };

  render() {
    return (
      <div className={style.saveContainer}>
        <Button type="primary" size="large" onClick={this.handleSave}>
          保存至草稿箱
        </Button>
      </div>
    );
  }


}

const stateToProps = state => {
  return {
    query: state["router"]["location"]["query"],
    editor: state["editor"],
    loading: state["loading"],
  };
};

export default connect(stateToProps)(SaveDrafts);
