import React, { PureComponent } from "react";
import styles from "./index.less";
import { message, Modal } from "antd";
import { connect } from "dva";
import { Scrollbars } from "react-custom-scrollbars";
import Upload from "utils/lib/Upload";
import env from "@/env";
import Cookies from "js-cookie";
import { TOKEN_NAME } from "@/types";
import { getVideoInfo, getImageInfo } from "utils";
import Storage from "utils/Storage";

class MaterialLibraryModel extends PureComponent {
  constructor(props) {
    super(props);
    this.state = {
      tagList: [
        {
          id: 2,
          text: "推荐视频",
        },
        {
          id: 1,
          text: "推荐图片",
        },
        {
          id: 6,
          text: "我的",
        },
      ],
    };
  }

  hideModal = () => {
    const { dispatch } = this.props;
    dispatch({
      type: "editor/save",
      payload: {
        isShowMaterialListModel: false,
      },
    });
  };

  handleTagLiClick = id => {
    const { dispatch } = this.props;

    dispatch({
      type: "materialListModel/fetch",
      payload: {
        asset_type: id,
      },
    });
  };

  renderTagList = () => {
    const { tagList } = this.state,
      { materialLibrarySelectType } = this.props["materialListModel"],
      renderLi = () => {
        return tagList.map(tag => {
          const { id, text } = tag,
            isActive = id === materialLibrarySelectType ? styles.active : "";

          return (
            <li
              key={id}
              className={`flex cursorPointer ${isActive}`}
              onClick={() => this.handleTagLiClick(id)}
            >
              {text}
            </li>
          );
        });
      };

    return <ul className={`${styles.tagListUl}`}>{renderLi()}</ul>;
  };

  handleFileClick = item => {
    const {
        currentGroup,
        currentMaterialIndex,
        isShowCurrentTransform,
        assetList,
        allMaterialByGroup,
      } = this.props["editor"],
      { dispatch } = this.props;

    if (!isShowCurrentTransform) {
      message.info("请选择需要替换的素材对象");
      return;
    }
    for (const groupKey in currentGroup) {
      if (currentGroup.hasOwnProperty(groupKey)) {
        let materialList = currentGroup[groupKey];
        materialList.map((asset, i) => {
          if (asset["key"] === currentMaterialIndex) {
            let newUrl = item["url"],
              isVideo = false;

            switch (item["source_type"]) {
              case "mp4":
              case "mov":
                if (newUrl.indexOf("static.atvideo.cc") !== -1) {
                  newUrl += "?vframe/png/offset/0";
                } else {
                  newUrl += "?x-oss-process=video/snapshot,t_0";
                }
                isVideo = true;
                break;
              default:
                newUrl = item["url"];
                break;
            }

            dispatch({
              type: "editor/replaceImage",
              payload: {
                material: item,
                url: newUrl,
                assetName: asset["assetName"],
                assetList: assetList,
                groupKey: groupKey,
                allMaterialByGroup: allMaterialByGroup,
                isVideo: isVideo,
                i: i,
              },
            });
          }
          return null;
        });
      }
    }
  };

  renderContent = () => {
    const formattingTime = function(t) {
      let NowTimeValue = t,
        nowM = parseInt((NowTimeValue % 3600) / 60 + ""),
        nowS = parseInt((NowTimeValue % 60) + "");

      if (nowM < 10) {
        nowM = "0" + nowM;
      }

      if (nowS < 10) {
        nowS = "0" + nowS;
      }

      return nowM + ":" + nowS;
    };

    let { materialList, materialLibrarySelectType, userMaterialList } = this.props[
        "materialListModel"
      ],
      actionListNode = null;

    if (materialLibrarySelectType === 6) {
      materialList = userMaterialList;
      // actionListNode = (
      //   <div className={`flex borderBox ${styles.actionListBox}`}>
      //     操作
      //   </div>
      // );
    }

    const renderNode = materialList.map(material => {
      let { id, source_type, url, name, width, height, duration } = material,
        node = null,
        videoDuration = null,
        widthDotIndex = width.indexOf("."),
        heightDotIndex = height.indexOf("."),
        durationDotIndex = duration.indexOf(".");

      if (widthDotIndex !== -1) {
        width = width.substring(0, widthDotIndex);
      }

      if (heightDotIndex !== -1) {
        height = height.substring(0, heightDotIndex);
      }

      if (durationDotIndex !== -1) {
        duration = duration.substring(0, durationDotIndex);
      }

      // 604800000
      // console.log(material);

      switch (source_type) {
        case "mp4":
        case "mov":
          videoDuration = formattingTime(Number(duration));
          node = (
            <>
              <video src={url} />
              <div className={`flex ${styles.playIcon}`}></div>
            </>
          );
          break;

        default:
          node = <img src={url} alt="materialImg" />;
          break;
      }

      return (
        <li key={id} className={`flex borderBox cursorPointer`}>
          <div className={`flex ${styles.topContent}`}>{node}</div>
          <div className={`${styles.bottomContent}`}>
            <div className={`${styles.moveContent}`}>
              <div className={`${styles.materialInfo}`}>
                <p className={`${styles.fileName}`}>{name}</p>
                <div className={`flex borderBox ${styles.sizeBox}`}>
                  <span>{videoDuration}</span>
                  <span>{`${width}*${height}`}</span>
                </div>
              </div>
              <div className={`flex borderBox ${styles.materialAction}`}>
                <button
                  className={`flex cursorPointer ${styles.useButton}`}
                  onClick={() => this.handleFileClick(material)}
                >
                  立即使用
                </button>
                {actionListNode}
              </div>
            </div>
          </div>
        </li>
      );
    });

    return {
      titleText: "推荐视频",
      renderNode: renderNode,
    };
  };

  handleFileChange = async e => {
    let upload = new Upload({
      getTokenUrl: env.getTokenUrl,
      jwt: Cookies.get(TOKEN_NAME),
      sourceHost: env.sourceHost,
    });

    const file = e.target.files[0];
    if (!file) return false;
    const { size, name } = file,
      nameDot = name.lastIndexOf(".");
    let saveFileName = "",
      fileType = "";

    if (size / 1024 / 1024 > 200) {
      message.info("文件大小大于200M!");
      return false;
    }

    if (nameDot !== -1) {
      saveFileName = name.substring(0, nameDot);
      fileType = name.substring(nameDot + 1).toLowerCase();
    } else {
      saveFileName = name;
    }

    if (
      fileType !== "jpg" &&
      fileType !== "jpeg" &&
      fileType !== "mp4" &&
      fileType !== "mov" &&
      fileType !== "png"
    ) {
      message.info("请上传以jpg,jpeg,mp4,mov,png结尾文件");
      return false;
    }

    let url = await upload.uploadFile(file),
      sourceInfo = null;

    if (fileType === "mp4" || fileType === "mov") {
      sourceInfo = await getVideoInfo(url);
    } else {
      sourceInfo = await getImageInfo(url);
    }

    if (sourceInfo === "获取信息失败!") {
      message.error("获取文件信息失败,请坚持文件是否损坏,或更换文件重新上传!", 10);
      return false;
    }

    const saveData = {
        id: new Date().getTime(),
        source_type: fileType,
        name: saveFileName,
        width: `${sourceInfo.width}`,
        height: `${sourceInfo.height}`,
        duration: `${sourceInfo.duration || "0.0"}`,
        url: url,
      },
      {
        dispatch,
        materialListModel: { userMaterialList },
      } = this.props;

    dispatch({
      type: "materialListModel/fetch",
      payload: {
        asset_type: 6,
      },
    });

    dispatch({
      type: "materialListModel/setUserLocal",
      payload: [...userMaterialList, saveData],
    });

    message.success("文件上传成功!");
  };

  render() {
    const { titleText, renderNode } = this.renderContent(),
      { isShowMaterialListModel } = this.props["editor"];

    return (
      <Modal
        width={946}
        footer={null}
        visible={isShowMaterialListModel}
        maskClosable={true}
        onCancel={this.hideModal}
        className={`${styles.materialLibraryModelContainer}`}
      >
        <div className={`flex ${styles.materialLibraryModel}`}>
          <div className={`${styles.tagList}`}>
            <h4 className={`${styles.title}`}>素材库</h4>
            {this.renderTagList()}
            <div className={`flex ${styles.localUploadContainer}`}>
              <label
                form="localFileUpload"
                className={`flex borderBox cursorPointer ${styles.localUpload}`}
              >
                <span className={`iconfont ${styles.uploadIcon}`}>&#xe697;</span>
                <span>本地上传</span>
                <input
                  id="localFileUpload"
                  className="dnHidden"
                  type="file"
                  onChange={e => this.handleFileChange(e)}
                />
              </label>
              <div className={`iconfont ${styles.localUploadHelp}`}>
                <span>&#xe68f;</span>
                <div className={`flex borderBox ${styles.helpInfo}`}>
                  通过本地上传视频，大小不超过200M，支持格式:
                  mp4、mov、单次只能上传一个。为了保证裁切质量，请上传大于4秒的视频。
                </div>
              </div>
            </div>
          </div>
          <div className={`${styles.contentContainer}`}>
            <h4 className={`flex borderBox ${styles.title}`}>{titleText}</h4>
            <div className={`${styles.scrollBox}`}>
              <Scrollbars>
                <ul className={`flex borderBox ${styles.contentUl}`}>{renderNode}</ul>
              </Scrollbars>
            </div>
          </div>
        </div>
      </Modal>
    );
  }
}

const stateToProps = state => {
  return {
    editor: state["editor"],
    loading: state["loading"],
    materialListModel: state["materialListModel"],
  };
};

export default connect(stateToProps)(MaterialLibraryModel);
