import React, { PureComponent } from "react";
import { connect } from "dva";
import { Modal } from "antd";
import styles from "./index.less";

class VideoClip extends PureComponent {

  constructor(props) {
    super(props);
    this.contentRef = React.createRef();
    this.cuttingRef = React.createRef();

    this.state = {
      needX: 0,
      arrOption: [],
      videoRef: null,
      totalTime: 0,
      containerWidth: 700,
      unit: 10,
    };

    this.disX = 0;
    this.X = 0;
    this.startTime = 0;
    this.endTime = 0;
  }

  textRenderFunction(seconds) {
    let mm;
    let ss;
    if (seconds == null || seconds <= 0) {
      return "00:00";
    }

    mm = (seconds / 60) | 0;
    ss = parseInt(seconds) - mm * 60;
    if (parseInt(mm) < 10) {
      mm = "0" + mm;
    }
    if (ss < 10) {
      ss = "0" + ss;
    }
    return mm + ":" + ss;
  }

  fnDown(e) {
    this.X = e.clientX - this.cuttingRef.current.offsetLeft;
    document.onmousemove = this.fnMove.bind(this);
    document.onmouseup = this.fnUp.bind(this);
    const { videoRef } = this.state;
    if (videoRef && !videoRef.paused) {
      videoRef.pause();
    }
  }

  fnMove(e) {
    let _this = this;
    let contentRef = this.contentRef.current.offsetWidth;
    let cuttingRef = this.cuttingRef.current.offsetWidth;
    this.disX = e.clientX - this.X;
    if (this.disX < 0) {
      this.setState({
        needX: 0,
      });
    } else if (this.disX > contentRef - cuttingRef) {
      this.disX = contentRef - cuttingRef;
    } else {
      this.setState({
        needX: _this.disX,
      });
    }
  }

  fnUp() {
    // 第三步：鼠标放开时document移除onmousemove事件
    document.onmousemove = null;
    document.onmouseup = null;
    const { videoRef } = this.state;
    if (videoRef && videoRef.paused) {
      videoRef.play();
    }
  }

  time(value) {
    let _this = this;
    let arr = [];
    for (let i = 0; i < value; i++) {
      if (i % 5 === 0) {
        let time = _this.textRenderFunction(i);
        // let text = document.createTextNode(time);
        // let divCreate = document.createElement("div");
        // divCreate.appendChild(text);
        // _this.tickRef.current.appendChild(divCreate);
        arr.push(time);
      }
    }
    this.setState({
      arrOption: arr,
    });
  }

  widthToTime(width) {
    return width / this.state.unit;
  }

  timeToWidth(duration) {
    return duration * this.state.unit;
  }

  closeModel = () => {
    const { dispatch } = this.props;
    dispatch({
      type: "editor/save",
      payload: {
        isShowVideoClip: false,
      },
    });
  };

  getImageUrl = () => {
    const { currentGroup, currentMaterialIndex, assetList } = this.props["editor"];
    for (const key in currentGroup) {
      if (currentGroup.hasOwnProperty(key)) {
        let list = currentGroup[key],
          i = 0,
          len = list.length;
        for (; i < len; i++) {
          let material = list[i];
          if (material["key"] === currentMaterialIndex) {
            return {
              imageUrl: assetList[material["assetName"]],
              material: material,
            };
          }

        }
      }
    }

    return {
      imageUrl: "",
      material: {},
    };
  };

  handleOnOK = (item) => {
    const { dispatch } = this.props;

    dispatch({
      type: "editor/changeGroupData",
      payload: {
        type: "videoClip",
        data: {
          startTime: this.startTime,
          endTime: this.endTime,
        },
      },
    });

    this.closeModel();

  };

  render() {
    const {
        needX, videoRef, totalTime, containerWidth,
      } = this.state,
      { imageUrl, material } = this.getImageUrl(),
      { parseManage: { fps } } = this.props["editor"],
      duration = material["asset"]["ui"]["duration"] / fps,
      currentTime = this.widthToTime(needX);

    this.startTime = currentTime;
    this.endTime = currentTime + duration;

    let url = imageUrl;
    if (imageUrl.lastIndexOf("?") !== -1) {
      url = imageUrl.substring(0, imageUrl.lastIndexOf("?"));
    }

    if (videoRef) {
      videoRef.currentTime = currentTime;
      videoRef.addEventListener("canplay", () => {
        if (totalTime !== videoRef.duration) {
          let totalTime = videoRef.duration;
          this.setState({
            totalTime: totalTime,
            unit: containerWidth / totalTime,
          });
        }
        return false;
      });

    }

    return (
      <Modal
        width={1063}
        visible={true}
        title="视频裁切"
        onCancel={this.closeModel}
        onOk={() => this.handleOnOK(material)}
      >
        <div className={styles.cutting}>

          <div className={`${styles.videoBox}`}>
            <video
              ref={ref => this.setState({ videoRef: ref })}
              src={url}
              muted
            />
          </div>
          <div className={`${styles.timeBox}`}>
            {this.textRenderFunction(this.startTime)}-{this.textRenderFunction(this.endTime)}
          </div>
          <div className={`${styles.timeTip}`}>
            起始时间到结束时间之外的视频会被裁剪
          </div>
          <div className={`${styles.totalTimeBox}`}>
            {this.textRenderFunction(totalTime)}
          </div>
          <div className={`${styles.totalTimeTip}`}>
            视频有效时长
          </div>

          <div className={`flex ${styles.tick_content}`}>
            <div
              className={styles.video_frame}
              style={{ width: `${containerWidth}px` }}
            >
              <div
                className={styles.content_frame}
                ref={this.contentRef}
                style={{ width: `${containerWidth}px` }}
              >
                <div
                  className={styles.cutting_frame}
                  ref={this.cuttingRef}
                  style={{ left: needX, width: `${this.timeToWidth(duration)}px`, maxWidth: `${containerWidth}px` }}
                  onMouseDown={this.fnDown.bind(this)}
                >
                  <div className={styles.translateIcon}/>
                  <div className={styles.cutting_region}/>
                  <div className={styles.translateIcon}/>
                </div>
              </div>
            </div>
          </div>
        </div>
      </Modal>
    );
  }
}

const stateToProps = state => {
  return {
    editor: state["editor"],
    loading: state["loading"],
  };
};

export default connect(stateToProps)(VideoClip);
