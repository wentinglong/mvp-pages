import React, { PureComponent } from "react";
import ReactCrop from "react-image-crop";
import "react-image-crop/dist/ReactCrop.css";
import { connect } from "dva";
import { Modal, message } from "antd";
import styles from "./index.less";
import Cookies from "js-cookie";
import Upload from "utils/lib/Upload";
import env from "@/env";
import { TOKEN_NAME } from "@/types";


class Tailoring extends PureComponent {

  constructor(props) {
    super(props);
    this.state = {
      crop: {
        unit: "%",
      },
    };

    this.corpFile = null;
  }

  onImageLoaded = image => {
    image.crossOrigin = "anonymous";
    this.imageRef = image;
    this.imageRef.width = 361;
  };

  onCropComplete = crop => {
    this.makeClientCrop(crop).then(res => {
      this.corpFile = res;
    });
  };

  onCropChange = (crop) => {
    this.setState({ crop });
  };

  async makeClientCrop(crop) {
    if (this.imageRef && crop.width && crop.height) {
      return await this.getCroppedImg(this.imageRef, crop, "newFile.jpeg");
    }
  }

  getCroppedImg(image, crop, fileName) {
    const canvas = document.createElement("canvas");
    const scaleX = image.naturalWidth / image.width;
    const scaleY = image.naturalHeight / image.height;
    canvas.width = crop.width;
    canvas.height = crop.height;
    const ctx = canvas.getContext("2d");
    image.crossOrigin = "anonymous";

    ctx.drawImage(
      image,
      crop.x * scaleX,
      crop.y * scaleY,
      crop.width * scaleX,
      crop.height * scaleY,
      0,
      0,
      crop.width,
      crop.height,
    );

    return new Promise((resolve) => {
      canvas.toBlob(blob => {
        if (!blob) {
          console.error("Canvas is empty");
          return;
        }
        blob.name = fileName;
        const file = new File([blob], fileName, { type: blob.type });
        resolve(file);
      });
    });
  }

  resetBtn = () => {
    this.corpFile = null;
    this.setState({
      crop: {
        unit: "%",
        width: 0,
        height: 0,
      },
    });

  };

  determineBtn = async () => {
    const { dispatch } = this.props;

    let upload = new Upload({
      getTokenUrl: env.getTokenUrl,
      jwt: Cookies.get(TOKEN_NAME),
      sourceHost: env.sourceHost,
    });
    if (!this.corpFile) {
      message.info("上传图片出错,请重新裁切!");
      return;
    }

    if (this.loading) {
      return;
    }
    this.loading = true;
    let imageUrl = await upload.uploadFile(this.corpFile);
    dispatch({
      type: "editor/save",
      payload: {
        isShowCorpImage: false,
      },
    });
    this.replayImage(imageUrl);

    this.loading = false;
  };

  replayImage = (newUrl) => {
    const {
        currentGroup, currentMaterialIndex, assetList, allMaterialByGroup,
      } = this.props["editor"],
      { dispatch } = this.props;

    for (const groupKey in currentGroup) {
      if (currentGroup.hasOwnProperty(groupKey)) {
        let materialList = currentGroup[groupKey];
        materialList.map((asset, i) => {
          if (asset["key"] === currentMaterialIndex) {
            dispatch({
              type: "editor/replaceImage",
              payload: {
                url: newUrl,
                assetName: asset["assetName"],
                assetList: assetList,
                groupKey: groupKey,
                allMaterialByGroup: allMaterialByGroup,
                i: i,
              },
            });
          }
          return null;
        });
      }
    }
  };

  getImageUrl = () => {
    const {
      allOriginMaterialByGroup, assetList, currentGroupIndex, currentMaterialIndex,
    } = this.props["editor"];

    let currentGroup = allOriginMaterialByGroup[currentGroupIndex],
      i = 0,
      len = currentGroup.length;

    for (; i < len; i++) {
      let material = currentGroup[i];
      if (material["key"] === currentMaterialIndex) {
        return {
          imageUrl: assetList[material["assetName"]],
          material: material,
        };
      }

    }

    return {
      imageUrl: "",
      material: {},
    };
  };

  hideModal = () => {
    const { dispatch } = this.props;
    dispatch({
      type: "editor/save",
      payload: {
        isShowCorpImage: false,
      },
    });
  };

  render() {
    const { crop } = this.state,
      { imageUrl, material } = this.getImageUrl();

    crop.aspect = material.originSize[0] / material.originSize[1];

    return (
      <Modal
        width={820} footer={null} visible={true} maskClosable={true}
        onCancel={this.hideModal}
      >
        <div className={styles.modal}>
          <div className={styles.tailoringLeft}>
            {imageUrl && (
              <ReactCrop
                src={imageUrl}
                crop={crop}
                ruleOfThirds
                onImageLoaded={this.onImageLoaded}
                onComplete={this.onCropComplete}
                onChange={this.onCropChange}
              />
            )}
            {/*<div className={styles.action}>*/}
            {/*  <ul>*/}
            {/*    <li>*/}
            {/*      <img src="#" />*/}
            {/*    </li>*/}
            {/*    <li>*/}
            {/*      <img src="#" />*/}
            {/*    </li>*/}
            {/*    <li>*/}
            {/*      <img src="#" />*/}
            {/*    </li>*/}
            {/*    <li>*/}
            {/*      <img src="#" />*/}
            {/*    </li>*/}
            {/*  </ul>*/}
            {/*</div>*/}
          </div>
          <div className={styles.tailoringright}>
            <h4>裁剪图片</h4>
            <div className={`flex ${styles.buttonBox}`}>
              <button onClick={this.resetBtn}>重置</button>
              <button onClick={this.determineBtn}>确定</button>
            </div>
          </div>
        </div>
      </Modal>
    );
  }
}

const stateToProps = state => {
  return {
    editor: state["editor"],
    loading: state["loading"],
  };
};

export default connect(stateToProps)(Tailoring);
