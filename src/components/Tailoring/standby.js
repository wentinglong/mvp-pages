import React, { PureComponent } from "react";
import { connect } from "dva";
import { Modal, message } from "antd";
import styles from "./standby.less";
import Cookies from "js-cookie";
import Upload from "utils/lib/Upload";
import env from "@/env";
import { TOKEN_NAME } from "@/types";
import Transform from "components/EditMain/Transform";
import { getImageInfo } from "utils";
import Affine from "utils/lib/Affine";


class Tailoring extends PureComponent {

  constructor(props) {
    super(props);
    this.state = {
      cropInfo: {},
      cropFile: null,
      cropDomRef: null,
      imageSize: [100, 100],
      isShowTransform: true,
    };

    this.scaleDist = {
      x: 1,
      y: 1,
    };

    this.rotateDist = 0;

    this.loading = false;

  }

  componentDidUpdate() {
    this.resetBtn();
  }

  renderTransform() {
    const { handleDragEnd, handleScaleEnd, handleRotateEnd } = this,
      { cropDomRef, isShowTransform } = this.state;

    if (isShowTransform) {
      return (
        <Transform
          target={cropDomRef}
          handleScaleEnd={handleScaleEnd}
          handleDragEnd={handleDragEnd}
          handleRotateEnd={handleRotateEnd}
          // handleScaleStart={handleTransformStart}
          // handleDragStart={handleTransformStart}
          // handleRotateStart={handleTransformStart}
        />
      );
    }

    return null;

  }

  handleDragEnd = ({ lastEvent }) => {
    if (lastEvent) {

    }
  };

  handleScaleEnd = ({ lastEvent }) => {
    if (lastEvent) {
      this.scaleDist.x *= lastEvent.dist[0];
      this.scaleDist.y *= lastEvent.dist[1];
    }
  };

  handleRotateEnd = ({ lastEvent }) => {
    if (lastEvent) {
      this.rotateDist += lastEvent.rotate;
    }
  };

  createCanvas = (size) => {
    const canvas = document.createElement("canvas"),
      context = canvas.getContext("2d");
    canvas.width = size[0];
    canvas.height = size[1];

    return {
      canvas: canvas,
      context: context,
    };
  };

  getCroppedImg(url, imageSize, scale, materialOriginSize, materialScale) {
    const { cropDomRef } = this.state,
      cropCanvas = this.createCanvas(materialOriginSize),
      wrapCanvas = this.createCanvas(imageSize),
      w = materialOriginSize[0],
      h = materialOriginSize[1],
      imgWidth = imageSize[0],
      imgHeight = imageSize[1],
      imageTransform = document.defaultView.getComputedStyle(cropDomRef, null).transform,
      matrixList = imageTransform.substring(imageTransform.indexOf("(") + 1, imageTransform.indexOf(")")).split(",");


    let a = {
        x: imgWidth / 2,
        y: imgHeight / 2,
      },
      p = {
        x: (imgWidth / 2) + (Number(matrixList[4]) / scale),
        y: (imgHeight / 2) + (Number(matrixList[5]) / scale),
      },
      s = {
        x: this.scaleDist.x,
        y: this.scaleDist.y,
      },
      r = this.rotateDist;

    let matrix = new Affine();
    matrix.set(a, p, s, r);
    wrapCanvas.context.setTransform(matrix.mA, matrix.mB, matrix.mC, matrix.mD, matrix.mE, matrix.mF);
    wrapCanvas.context.drawImage(cropDomRef, 0, 0, imgWidth, imgHeight);
    cropCanvas.context.drawImage(
      wrapCanvas.canvas,
      (imgWidth / 2) - (w * materialScale / scale / 2),
      (imgHeight / 2) - (h * materialScale / scale / 2),
      w * materialScale / scale,
      h * materialScale / scale,
      0,
      0,
      w,
      h,
    );


    return new Promise(resolve => {
      cropCanvas.canvas.toBlob(blob => {
        if (!blob) {
          console.error("Canvas is empty");
          return;
        }
        const file = new File([blob], "cropImage.png", { type: blob.type });
        resolve(file);
      });
    });


  }

  replayImage = (newUrl) => {
    const {
        currentGroup, currentMaterialIndex, assetList, allMaterialByGroup,
      } = this.props["editor"],
      { dispatch } = this.props;

    for (const groupKey in currentGroup) {
      if (currentGroup.hasOwnProperty(groupKey)) {
        let materialList = currentGroup[groupKey];
        materialList.map((asset, i) => {
          if (asset["key"] === currentMaterialIndex) {
            dispatch({
              type: "editor/replaceImage",
              payload: {
                url: newUrl,
                assetName: asset["assetName"],
                assetList: assetList,
                groupKey: groupKey,
                allMaterialByGroup: allMaterialByGroup,
                i: i,
              },
            });
          }
          return null;
        });
      }
    }
  };

  resetBtn = () => {
    this.setState({
      isShowTransform: false,
    }, () => {
      this.setState({
        isShowTransform: true,
      });
    });
  };

  determineBtn = async (url, size, scale, materialOriginSize, materialScale) => {
    if (this.loading) {
      message.info("文件正在上传,请勿重复点击!");
      return false;
    }
    this.loading = true;
    const { dispatch } = this.props,
      file = await this.getCroppedImg(url, size, scale, materialOriginSize, materialScale),
      upload = new Upload({
        getTokenUrl: env.getTokenUrl,
        jwt: Cookies.get(TOKEN_NAME),
        sourceHost: env.sourceHost,
      }),
      imageUrl = await upload.uploadFile(file);


    dispatch({
      type: "editor/save",
      payload: {
        isShowCorpImage: false,
      },
    });
    this.replayImage(imageUrl);
    this.loading = false;
  };

  getWidthScale(height, size) {
    let scale = height / size[1],
      width = size[0] * scale;
    return {
      scale: scale,
      width: width,
    };
  }

  setImageSize(imageUrl) {
    getImageInfo(imageUrl).then(({ width, height }) => {
      const { imageSize } = this.state;
      if (imageSize[0] !== width || imageSize[1] !== height) {
        this.setState({
          imageSize: [width, height],
        });
      }
    });
  }

  getImageUrl = () => {
    const {
      allOriginMaterialByGroup, assetList, currentGroupIndex, currentMaterialIndex,
    } = this.props["editor"];

    let currentGroup = allOriginMaterialByGroup[currentGroupIndex],
      i = 0,
      len = currentGroup.length;

    for (; i < len; i++) {
      let material = currentGroup[i];
      if (material["key"] === currentMaterialIndex) {
        return {
          imageUrl: assetList[material["assetName"]],
          material: material,
        };
      }

    }

    return {
      imageUrl: "",
      material: {},
    };
  };

  hideModal = () => {
    const { dispatch } = this.props;
    dispatch({
      type: "editor/save",
      payload: {
        isShowCorpImage: false,
      },
    });
  };


  render() {
    const { imageSize, isShowTransform } = this.state,
      { imageUrl, material } = this.getImageUrl(),
      materialOriginSize = material.originSize,
      containerHeight = 460,
      materialHeight = 260,
      { scale, width: containerWidth } = this.getWidthScale(containerHeight, imageSize),
      { scale: materialScale, width: materialContainerWidth } = this.getWidthScale(materialHeight, materialOriginSize);

    this.setImageSize(imageUrl);

    return (
      <Modal
        width={containerWidth + 260} footer={null} visible={true} maskClosable={true}
        onCancel={this.hideModal}
      >
        <div className={styles.modal}>
          <div
            className={styles.tailoringLeft}
            style={{
              height: containerHeight,
              width: containerWidth,
            }}
          >
            <div
              className={styles.cropBorderBox}
              style={{
                height: materialHeight,
                width: materialContainerWidth,
              }}
            />
            {
              isShowTransform && <img
                className={styles.imageBox}
                ref={ref => this.setState({ cropDomRef: ref })}
                src={imageUrl}
                crossOrigin="anonymous"
                style={{
                  height: containerHeight,
                  width: containerWidth,
                }}
                alt="asset"
              />
            }
            {this.renderTransform()}
          </div>
          <div className={styles.tailoringright}>
            <h4>裁剪图片</h4>
            <div className={`flex ${styles.buttonBox}`}>
              <button onClick={this.resetBtn}>重置</button>
              <button
                onClick={() => this.determineBtn(
                  imageUrl, imageSize, scale, materialOriginSize,
                  materialScale,
                )}>确定
              </button>
            </div>
          </div>
        </div>
      </Modal>
    );
  }
}

const stateToProps = state => {
  return {
    editor: state["editor"],
    loading: state["loading"],
  };
};

export default connect(stateToProps)(Tailoring);
