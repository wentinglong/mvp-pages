// src/env.js 中的相关配置是 ve demo 自己使用的测试api, 用户根据自己的业务更换成自己的 api 链接
// 有一些逻辑比如获取上传 token 是为了演示这个项目而设置的, 用户需要根据使用的 oss 自行提供获取 token 的 api
const { host } = window.location,
  testHost = `//phpsdk.atvideo.cc`,
  onLineHost = `//phpsdk.atvideo.cc`,
  veLeapTestHost = `//test02.veleap.com`,
  veLeapOnLineHost = `//veleap.com`,
  getOssTokenApi = `/demo/main/get_oss_policy`,
  isLocal =
    host.indexOf("localhost") !== -1 ||
    host.indexOf("127.0.0.1") !== -1 ||
    host.indexOf("test") !== -1 ||
    host.indexOf("192.168.") !== -1;

let env = {
  // api 所使用的 host
  host: testHost,
  // 展示项目用到的 用户不用关心
  veLeapHost: veLeapTestHost,
  // 上传文件和渲染 api 返回的是相对路径 需要用户自己 根据自己的 oss 进行修改
  sourceHost: "//web-demo-source.atvideo.cc/",
  // 获取 oss 上传 token 的 api
  getTokenUrl: "http://phpsdk.atvideo.cc/TemplateDemo.php?act=get_upload_key",
};

if (isLocal) {
  env.host = testHost;
  env.veLeapHost = veLeapTestHost;
} else {
  env.host = onLineHost;
  env.veLeapHost = veLeapOnLineHost;
}

// env.veLeapHost = veLeapOnLineHost;
// env.host = onLineHost;
env.getTokenUrl = `${env.host}${getOssTokenApi}`;

export default env;
