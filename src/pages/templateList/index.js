import React, { useState } from "react";
import { connect } from "dva";
import router from "umi/router";
import { Pagination } from "antd";
import styles from "./index.less";
import Video from "./Video";

function TemplateList(props) {

  const [isOpenTagList, setOpenTagList] = useState(false);
  // globalLoading
  const { templateList, total, tagList, category_id, currentPage } = props.templateList,
    { dispatch } = props,
    paginationProps = {
      showQuickJumper: true,
      defaultPageSize: 16,
      defaultCurrent: 1,
      total: total,
      onChange(currentPage) {
        dispatch({
          type: "templateList/fetch",
          payload: {
            page_size: 18,
            page: currentPage,
          },
        });
      },
    };
  // loading = (
  //   <div className={`flex`}>
  //     <Spin tip="Loading..."/>
  //   </div>
  // );

  const changeTagList = () => {
    setOpenTagList(!isOpenTagList);
  };


  let topTagList = [],
    bottomTagList = [],
    isShowLoadMore = false;


  if (tagList.length > 12) {
    topTagList = tagList.slice(0, 12);
    bottomTagList = tagList.slice(12, 20);
    isShowLoadMore = true;
  } else {
    topTagList = tagList;
  }

  //渲染标签
  const renderTagList = (list) => {

    const renderLi = (key, title) => {
        const handleClick = () => {
          dispatch({
            type: "templateList/fetch",
            payload: {
              page_size: 18,
              page: currentPage,
              template_group: key,
              category_id: key,
            },
          });
        };

        return (
          <li
            key={key}
            onClick={handleClick}
            className={key === category_id ? styles.active : ""}
          >
            {title}
          </li>
        );
      },
      virtualDomList = [];


    list.map(item => {
      virtualDomList.push(renderLi(item["category_id"], item["category_name"]));
      return null;
    });


    return virtualDomList;
  };

  // 模板点击事件
  const handleTemplateClick = (templateItem) => {
    console.log(templateItem);
    dispatch({
      type: "editor/save",
      payload: {
        pageLoading: true,
        pageLoadingText: "正在加载模板文件",
      },
    });
    router.push({
      pathname: "/editor",
      query: {
        templateId: templateItem.id,
      },
    });
  };

  //渲染模板列表
  const renderTemplateList = (list = []) => {
    const renderLi = (item) => {
      let templateName = item["template_name"],
        tag = item.tag || "热门",
        coverVideo = item["template_demo_video"],
        coverImage = item["template_cover_image"];

      return (
        <li key={item.id} onClick={() => handleTemplateClick(item)}>
          <Video
            url={coverVideo}
            coverImage={coverImage}
          />
          <button className={`flex ${styles.immediatelyMake}`}>立即制作</button>
          <div className={`flex borderBox ${styles.textInfo}`}>
            <div className={`${styles.templateTitle}`} title={templateName}>{templateName}</div>
            <div className={`flex ${styles.tag}`}>{tag}</div>
          </div>
        </li>
      );
    };

    let virtualDomList = [];
    virtualDomList = list.map(item => renderLi(item));

    return virtualDomList;
  };

  //分类 展开收起
  const renderProps = {
    dnClass: isOpenTagList ? "dnFlexShow" : "dnHidden",
    text: isOpenTagList ? "收起" : "展开",
  };

  if (!isShowLoadMore) {
    renderProps.text = "";
  }

  return (
    <div className={styles.container}>
      <h3 className={styles.title}>标准模板</h3>
      {/* 分类 */}
      <div className={`flex borderBox ${styles.filterBox}`}>
        <span className={styles.filterTitle}>分类：</span>
        <span className={styles.actionText} onClick={changeTagList}>{renderProps.text}</span>
        <div className={styles.classListBox}>
          <ul className={`flex ${styles.filterTagList}`}>
            {renderTagList(topTagList)}
          </ul>
          <ul className={`flex ${styles.filterTagList} ${styles.removePadding} ${renderProps.dnClass}`}>
            {renderTagList(bottomTagList)}
          </ul>
        </div>
      </div>

      {/* 模板列表 */}
      <div className={`${styles.templateListContainer}`}>
        <ul className={`flex ${styles.templateList}`}>
          {renderTemplateList(templateList)}
        </ul>
      </div>

      {/* 分页 */}
      <div className={`flex ${styles.pagination}`}>
        <Pagination {...paginationProps}/>
      </div>

    </div>
  );
}


const stateToProps = state => {
  return {
    globalLoading: state["loading"]["global"],
    templateList: state["templateList"],
  };
};

export default connect(stateToProps)(TemplateList);

