import React, { PureComponent } from "react";
import styles from "../index.less";

class Video extends PureComponent {

  constructor(props) {
    super(props);
    this.state = {
      videoRef: null,
      isShowVideo: false,
    };
  }


  handleEnter = () => {
    const { videoRef } = this.state;
    if (videoRef && (videoRef && videoRef.paused && videoRef.readyState === 4)) {
      this.setState({
        isShowVideo: true,
      });
      videoRef.play();
    }
  };


  handleLeave = () => {
    const { videoRef } = this.state;
    if (videoRef && (videoRef && !videoRef.paused)) {
      this.setState({
        isShowVideo: false,
      });
      videoRef.pause();
    }
  };


  render() {
    const { url, coverImage } = this.props,
      { isShowVideo } = this.state;

    return (
      <div
        className={`flex ${styles.imageContent}`}
        onMouseEnter={this.handleEnter}
        onMouseLeave={this.handleLeave}
      >
        {
          coverImage && <img
            style={{
              zIndex: isShowVideo ? 1 : 2,
              visibility: isShowVideo ? "hidden" : "visible",
            }}
            src={coverImage}
            alt="封面图"
          />
        }
        <video
          ref={ref => this.setState({ videoRef: ref })}
          src={url}
          style={{
            zIndex: isShowVideo ? 2 : 1,
            visibility: isShowVideo ? "visible" : "hidden",
          }}
          muted
          loop
        />
      </div>
    );
  }

}

export default Video;
