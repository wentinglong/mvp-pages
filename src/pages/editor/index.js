import React, { PureComponent } from "react";
import { Spin } from "antd";
import { connect } from "dva";
import styles from "./index.less";
import MaterialLibrary from "components/MaterialLibrary";
import EditGroups from "components/EditGroups";
import EditWindow from "components/EditWindow";
import EditMain from "components/EditMain";
import Tailoring from "components/Tailoring/standby";
import MaterialLibraryModel from "components/MaterialLibraryModel";
import VideoClip from "components/VideoClip";
import SaveDrafts from "components/SaveDrafts";

class Editor extends PureComponent {

  getCurrentGroupAccessoryImage() {

    const {
      parseManage, currentGroup, backgroundImageList, prospectImageList,
    } = this.props["editor"];

    let backgroundImage = "",
      prospectImage = "",
      groupKey = "";
    for (const key in currentGroup) {
      if (currentGroup.hasOwnProperty(key)) {
        backgroundImage = parseManage.groups[key].backgroundImage;
        prospectImage = parseManage.groups[key].prospectImage;
        groupKey = key;
      }
    }

    return {
      backgroundImage: backgroundImageList[backgroundImage],
      prospectImage: prospectImageList[prospectImage],
      groupKey: groupKey,
    };

  }

  renderLoading(status, text) {
    if (!status) return null;
    return (
      <div className={`flex ${styles.loadingEditorContainer}`}>
        <Spin size="large" tip={`${text}...`}/>
      </div>
    );
  }

  renderCropBox() {
    const { isShowCorpImage } = this.props["editor"];
    if (!isShowCorpImage) return null;
    return (
      <Tailoring/>
    );
  }

  renderVideoClipBox() {
    const { isShowVideoClip } = this.props["editor"];
    if (!isShowVideoClip) return null;
    return (
      <VideoClip/>
    );
  }

  renderSaveDrafts() {
    const { isVeLeapTest } = this.props["editor"];
    if (!isVeLeapTest) return null;

    return (
      <SaveDrafts/>
    );
  }

  render() {
    const {
        horizontalVersion, showStyleInfo, scale, isOpenMaterialList,
        editorContainerHeight, totalTime, backgroundImageList, prospectImageList,
        assetList, allMaterialByGroup, parseManage, pageLoading, pageLoadingText,
      } = this.props["editor"],
      accessoryImage = this.getCurrentGroupAccessoryImage();

    return (
      <div className={`borderBox ${styles.editorContainer}`}>
        {this.renderLoading(pageLoading, pageLoadingText)}
        {this.renderCropBox()}
        {this.renderVideoClipBox()}
        {this.renderSaveDrafts()}
        <MaterialLibraryModel/>
        <MaterialLibrary/>
        <EditGroups
          groups={allMaterialByGroup}
          horizontalVersion={horizontalVersion}
          containerHeight={showStyleInfo.groupHeight}
          isOpenMaterialList={isOpenMaterialList}
          editorContainerHeight={editorContainerHeight}
          totalTime={totalTime}
          backgroundImageList={backgroundImageList}
          prospectImageList={prospectImageList}
          assetList={assetList}
          allMaterialByGroup={allMaterialByGroup}
          parseManage={parseManage}
          currentGroupKey={accessoryImage.groupKey}
        />
        <EditWindow/>
        <EditMain
          isHorizontalVersion={horizontalVersion}
          containerHeight={showStyleInfo.containerHeight}
          scale={scale.container}
          backgroundImage={accessoryImage.backgroundImage}
          prospectImage={accessoryImage.prospectImage}
        />
      </div>
    );
  }

}


const stateToProps = (state) => {
  let loading = state["loading"]["global"];
  return {
    loadingEffects: loading,
    editor: state["editor"],
  };
};

export default connect(stateToProps)(Editor);
