import React, { useEffect, useState } from "react";
import { connect } from "dva";
import styles from "./index.less";
import axios from "axios";
import env from "@/env";

function VideoPlay(props) {
  const [loading, setLoading] = useState(true),
    [url, setUrl] = useState(""),
    { orderId, video_url } = props.location.query;


  useEffect(() => {
    let timerId = -1;
    const pollVideoIsLoadSuccess = (id) => {
      return new Promise(resolve => {
        timerId = setInterval(async () => {
          const { data } = await axios({
            url: `${env.host}/TemplateDemo.php?act=get_status&record_id=${orderId}`,
            method: "GET",
            data: {
              order_id: id,
            },
          });
          
          if (data.errno !== 0) {
            clearInterval(timerId);
          }

          if (data.data.status === "1") {
            clearInterval(timerId);
            resolve(`${env.sourceHost}${video_url}`);
          }


        }, 500);
      });

    };

    pollVideoIsLoadSuccess(orderId).then((url) => {
      setLoading(false);
      setUrl(url);
    });

    return () => {
      clearInterval(timerId);
    };

  }, [orderId, video_url]);


  return loading ? (
    <div className={styles.content}>
      <div className={styles.loadingBox}>
        <div className={`flex ${styles.loaderContainer}`}>
          <div className={`${styles.eight}`}/>
        </div>
      </div>
      <p>处理中...</p>
    </div>
  ) : (
    <div className={styles.videoPlay}>
      <video
        src={url}
        style={{ width: "1109px", height: "625px", outline: "none" }}
        controls
      />
    </div>
  );
}

const stateToProps = state => {
  return {
    editor: state["editor"],
    loading: state["loading"],
  };
};

export default connect(stateToProps)(VideoPlay);

