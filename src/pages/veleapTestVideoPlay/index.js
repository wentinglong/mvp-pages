import React, { PureComponent } from "react";
import { connect } from "dva";
import styles from "./index.less";
import axios from "axios";
import env from "@/env";
import { Button, Modal, message } from "antd";
import { openNotification } from "utils/userPrompt";
import E from "wangeditor";
import Upload from "utils/lib/Upload";
import Cookies from "js-cookie";
import { TOKEN_NAME } from "@/types";

class VeLeapTestVideoPlay extends PureComponent {

  editor = { config: {} };
  timerId = -1;
  upload = new Upload({
    getTokenUrl: env.getTokenUrl,
    jwt: Cookies.get(TOKEN_NAME),
    sourceHost: env.sourceHost,
  });

  constructor(props) {
    super(props);
    this.state = {
      loading: true,
      url: "",
      isShowEditor: false,
    };
  }

  createEditor = () => {
    this.editor = new E("#editTextInfo");
    this.editor.config.menus = [
      "head",
      "bold",
      "image",
      "video",
    ];
    this.editor.config.showFullScreen = false;
    this.editor.config.showMenuTooltips = false;
    this.editor.config.showLinkVideo = false;
    this.editor.config.showLinkImg = false;
    this.editor.config.uploadImgMaxLength = 1;
    this.editor.config.customAlert = function(s, t) {
      switch (t) {
        case "error":
          message.error(s, 6);
          break;

        case "warning":
          message.error(s, 6);
          break;

        default:
          message.info(s, 6);
          break;
      }
    };

    this.editor.config.customUploadImg = async (resultFiles, insertImgFn) => {
      const file = resultFiles[0],
        url = await this.upload.uploadFile(file);
      insertImgFn(url);
    };


    this.editor.create();


  };

  uploadVideo = async (e) => {
    message.info("视频正在上传...");
    const file = e.target.files[0],
      url = await this.upload.uploadFile(file);
    this.editor.cmd.do("insertHTML", "<video controls src=\"" + url + "\">不支持video</video>"); //插入视频
    console.log(url);
    message.info("视频上传成功!");
  };

  showRejectContainer = () => {
    Modal.confirm({
      title: "确定驳回吗？",
      okText: "确认",
      cancelText: "取消",
      onOk: () => {
        this.setState({
          isShowEditor: true,
        });
      },
    });

  };

  renderResult = () => {
    const { loading, url } = this.state,
      { props, commitRenderResult, showRejectContainer, submitResult } = this,
      { i, test_adopt } = props.location.query;
    if (loading) {
      return (
        <div className={styles.content}>
          <div className={styles.loadingBox}>
            <div className={`flex ${styles.loaderContainer}`}>
              <div className={`${styles.eight}`}/>
            </div>
          </div>
          <p>处理中...</p>
        </div>
      );
    }

    if (!loading) {
      return (
        <div className={styles.videoPlay}>
          <video
            src={url}
            style={{ width: "1109px", height: "625px", outline: "none" }}
            controls
          />
          {i === "1" && (
            <div className={styles.btnContainer}>
              <Button
                type="primary"
                size="large"
                onClick={commitRenderResult}>
                提交渲染结果
              </Button>
            </div>
          )}

          {(i === "2" && test_adopt === "2") && (
            <div className={styles.demandBtnContainer}>
              <Button
                style={{
                  marginRight: 20,
                  width: 108,
                  height: 42,
                  letterSpacing: "-2px",
                }}
                type="primary"
                onClick={() => submitResult(1)}
              >
                同意
              </Button>
              <Button
                style={{
                  width: 108,
                  height: 42,
                  letterSpacing: "-2px",
                }}
                onClick={showRejectContainer}
              >
                驳回
              </Button>
            </div>
          )}
        </div>
      );
    }

  };

  pollVideoIsLoadSuccess = (id) => {
    return new Promise(resolve => {
      this.timerId = setInterval(async () => {

        const { data } = await axios({
          url: `${env.host}/demo/public/get_status`,
          method: "POST",
          data: {
            order_id: id,
          },
        });

        if (data.errno !== 0) {
          clearInterval(this.timerId);
        }

        if (data.data.order.status === 1) {
          clearInterval(this.timerId);
          resolve(`${env.sourceHost}${data.data.order["video_url"]}`);
        }
      }, 500);

    });

  };

  commitRenderResult = () => {
    const { props, state, stylistToVeleap } = this,
      { url } = state,
      { v, pid, project_id } = props.location.query;
    const postData = {
      process_id: pid,
      status: "3",
      check_status: "2",
      token: v,
      render_result_video: url,
    };

    axios({
      url: `${env.veLeapHost}/web/process/commit`,
      method: "POST",
      data: postData,
    }).then(res => {
      const { data } = res;
      if (data["errno"] !== 0) {
        openNotification("提交失败", data["errormsg"], "error");
      } else {
        stylistToVeleap(project_id);
        openNotification("提交成功", "", "success");
      }
    });

  };

  stylistToVeleap = (project_id) => {
    console.log(project_id);
    window.location.href = `${env.veLeapHost}/web/design/upload?project_id=${project_id}`;
  };

  toVeleap = (project_id) => {
    window.location.href = `${env.veLeapHost}/web/task/project?page=1&page_size=6&project_id=${project_id}&detail_status=3&is_show=1`;
  };

  submitResult = (type) => {
    const { props, toVeleap } = this,
      { v, project_id, process_id } = props.location.query,
      postData = {
        process_id: process_id,
        token: v,
        is_adopt: type,
      };

    if (type === 2) {
      postData.opinion = this.editor.txt.html();
      if (!postData.opinion) {
        openNotification("提交失败", "请填写驳回理由!", "error");
        return;
      }
    }


    axios({
      url: `${env.veLeapHost}/web/process/adopt`,
      method: "POST",
      data: postData,
    }).then(res => {
      const { data } = res;
      console.log(data);

      if (data["errno"] !== 0) {
        if (data["errno"] === 40107) {
          toVeleap(project_id);
        } else {
          openNotification("提交失败", data["errormsg"], "error");
        }
      } else {
        toVeleap(project_id);
      }
    });

  };

  getEditorStyle = () => {
    const { isShowEditor } = this.state,
      styles = {
        display: "none",
      };

    if (isShowEditor) {
      styles.display = "block";
    }

    return styles;
  };

  componentDidMount() {
    const { createEditor, props, pollVideoIsLoadSuccess } = this,
      { orderId, i } = props.location.query;

    if (i === "2") {
      createEditor();
    }

    pollVideoIsLoadSuccess(orderId).then((url) => {
      this.setState({
        url: url,
        loading: false,
      });
    });

  }

  render() {
    const { renderResult, getEditorStyle, uploadVideo, submitResult } = this;

    return (
      <>
        {renderResult()}
        <div
          className={styles.editContentContainer}
          style={getEditorStyle()}
        >
          <div id="editTextInfo"/>
          <div className={styles.submitRejectContent}>
            <Button
              style={{
                width: 108,
                height: 42,
                letterSpacing: "-2px",
              }}
              type="primary"
              onClick={() => submitResult(2)}
            >
              提交
            </Button>

          </div>
          <label className={styles.uploadVideo} htmlFor="uploadVideo"/>
          <input id="uploadVideo" type="file" style={{ display: "none" }} accept="video/*" onChange={uploadVideo}/>
        </div>
      </>
    );

  }


}


const stateToProps = state => {
  return {
    editor: state["editor"],
    loading: state["loading"],
  };
};

export default connect(stateToProps)(VeLeapTestVideoPlay);

