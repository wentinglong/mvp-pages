import { _ } from "utils";

export const getToken = async data => _("/demo/public/gen_token", data);

export const getTemplateInfo = async data => _("/TemplateDemo.php", data, "get");

export const submitRender = async data => _("/TemplateDemo.php?act=start_render", data);

export const getVeLeapTemplateInfo = async data => _("/leap/designer/test_template_detail", data, "post", true);

export const saveVeLeapEdit = async data => _("/leap/designer/save_record_json", data, "post", true);




